<?php include_once "header.inc.php"; ?>

<div class="row wrapper border-bottom page-heading">

   <div class="col-lg-10">

      <h2><i class="fa fa-cog"></i> Configuraci&oacute;n / Productos</h2>

   </div>

   <div class="col-lg-2">

   </div>

</div>

<div class="wrapper wrapper-content">

<div class="row">

<div class="col-md-12">

   <div class="ibox-content">

      <div class="tabs-container">

         <ul class="nav nav-tabs">

            <li class="active"><a data-toggle="tab" href="#tab-1"> Vending</a></li>

            <li class=""><a data-toggle="tab" href="#tab-2">OCS</a></li>

         </ul>

         <div class="tab-content">

            <div id="tab-1" class="tab-pane active">

			<div class="row">

			   		 <div class="row" style="margin-left:0px; margin-top:15px">

                              <div class="form-group col-lg-2" >

                                 <button type="submit" class="btn btn-primary block full-width m-b btn-lg" data-toggle="modal" data-target="#myModal5"><i class="fa fa-plus" aria-hidden="true"></i> A&ntilde;adir producto</button>

                              </div>

                     </div>

					 

					  <div class="col-lg-12">

						 <div class="ibox float-e-margins">

							<div class="table-responsive">

							       <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">

								  <thead>

									 <tr>

										<th>No.</th>

										<th>Producto</th>

										<th style="text-align:center">Opciones</th>

									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=80; $i++){ ?>

									 <tr class="gradeC">

										<td>1</td>

										<td>Producto en vending </td>

										<td class="center" style="text-align:center"><span class="label label-primary" style="cursor:pointer" data-toggle="modal" data-target="#myModal5">Editar</span></td>

									 </tr>

									 <?php }?>

								  </tbody>
									 <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
							   </table>

							</div>

							

						 </div>

                  	</div>

				</div>



				

			

			

			</div>

			<div id="tab-2" class="tab-pane">

			

				<div class="row">

			   		 <div class="row" style="margin-left:0px; margin-top:15px">

                              <div class="form-group col-lg-2" >

                                <button type="submit" class="btn btn-primary block full-width m-b btn-lg" data-toggle="modal" data-target="#myModal5"><i class="fa fa-plus" aria-hidden="true"></i> A&ntilde;adir producto</button>


                              </div>

                     </div>

					 

					  <div class="col-lg-12">

						 <div class="ibox float-e-margins">

							<div class="table-responsive">

							      <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">

								  <thead>

									 <tr>

										<th>No.</th>

										<th>Producto</th>

										<th style="text-align:center">Opciones</th>

									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=80; $i++){ ?>

									 <tr class="gradeC">

										<td>1</td>

										<td>Producto en OCS </td>

										<td class="center" style="text-align:center"><span class="label label-primary" style="cursor:pointer" data-toggle="modal" data-target="#myModal5">Editar</span></td>

									 </tr>

									 <?php }?>

								  </tbody>
								 <tfoot>
                                <tr>
									<td colspan="2"><small>Mostrando 1 de 1</small></td>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
							   </table>

							</div>

							

						 </div>

                  	</div>
					

				</div>
				<div class="row">
					<div class="col-lg-8" style="margin-top:-20px">
							<div><h2>Grupos</h2></div>
						
						  <div class="col-lg-12">

						 <div class="ibox float-e-margins">

							<div class="table-responsive">

							  <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">

								  <thead>

									 <tr>

										<th>No.</th>

										<th>Grupo</th>
										<th>Productos</th>
										<th>Precio m&iacute;nimo</th>
										<th>Precio m&aacute;ximo</th>

										<th style="text-align:center">Opciones</th>

									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=10; $i++){ ?>

									 <tr class="gradeC">

										<td>1</td>

										<td>Grupo en vending </td>
										<td>Productos</td>
										<td>Precio m&iacute;nimo</td>
										<td>Precio m&aacute;ximo</td>

										<td class="center" style="text-align:center"><span class="label label-primary" style="cursor:pointer" data-toggle="modal" data-target="#myModalGrupo">Editar</span></td>

									 </tr>

									 <?php }?>

								  </tbody>
								   <tfoot>
<tr>
<td colspan="2"><small>Mostrando 1 de 1</small></td>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
								</tfoot>
							   </table>

							</div>

							

						 </div>

                  	</div>

						
						
					</div>
					<div class="col-lg-4" style="margin-top:-20px">
						<div><h2>Tipos</h2></div>
						
						  <div class="col-lg-12">

						 <div class="ibox float-e-margins">

							<div class="table-responsive">

							   <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">

								  <thead>

									 <tr>

										<th>No.</th>

										<th>Tipos</th>
										<th>Posici&oacute;n</th>

										<th style="text-align:center">Opciones</th>

									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=10; $i++){ ?>

									 <tr class="gradeC">

										<td>1</td>

										<td>Tipos en vending </td>
										<td>Posici&oacute;n</td>

										<td class="center" style="text-align:center"><span class="label label-primary" style="cursor:pointer" data-toggle="modal" data-target="#myModalTipo">Editar</span></td>

									 </tr>

									 <?php }?>

								  </tbody>
								   <tfoot>
<tr>
									<td colspan="2"><small>Mostrando 1 de 1</small></td>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
								</tfoot>
							   </table>

							</div>

							

						 </div>

                  	</div>

						
						
					</div>
					<div class="col-lg-5" style="margin-top:-20px">
						<div><h2>Categor&iacute;as</h2></div>
						  <div class="col-lg-12">

						 <div class="ibox float-e-margins">

							<div class="table-responsive">

							   <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">

								  <thead>

									 <tr>

										<th>No.</th>

										<th>Categor&iacute;a</th>
										<th>Posici&oacute;n</th>

										<th style="text-align:center">Opciones</th>

									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=10; $i++){ ?>

									 <tr class="gradeC">

										<td>1</td>

										<td>Categor&iacute;a en vending </td>
										<td>Posici&oacute;n</td>

										<td class="center" style="text-align:center"><span class="label label-primary" style="cursor:pointer" data-toggle="modal" data-target="#myModalCategoria">Editar</span></td>

									 </tr>

									 <?php }?>

								  </tbody>
								   <tfoot>
<tr>
									<td colspan="2"><small>Mostrando 1 de 1</small></td>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
								 <tfoot>
							   </table>

							</div>

							

						 </div>

                  	</div>

						
						
					</div>
				</div>
			

			

			</div>

		 </div>

	 </div>

	</div>

</div>



<div class="modal inmodal fade" id="myModalGrupo" tabindex="-1" role="dialog"  aria-hidden="true">

   <div class="modal-dialog modal-lg">

      <div class="modal-content">

         <div class="modal-header" style="text-align:left">

            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

            <h4 class="modal-title">Crear/Editar Grupo</h4>

         </div>

         <div class="modal-body">

       	 <form class="m-t" role="form" action="" required="" method="post">

             <div class="row" >

                 <div class="col-lg-12">

         	         <div class="form-group">

                      <label>Productos </label>

                      <input id="txtProductos" name="txtProductos" type="text" class="form-control input-sm" required="">

                     </div>   

				</div>
				 <div class="col-lg-12">

         	         <div class="form-group">

                      <label>Precios </label>

                    	<div style="border:solid 1px #eee; padding-top:10px; padding-left:10px; padding-right:10px">
						
							 <div class="row">

				                 <div class="col-lg-12">

									 <div class="form-group">
				
									  <label>M&iacute;nimo de producto </label>
				
									  <input id="txtProductos" name="txtProductos" type="text" class="form-control input-sm" required="">
				
									 </div>   
				
								</div>
								<div class="col-lg-12">

									 <div class="form-group">
				
									  <label>M&aacute;ximo de producto </label>
				
									  <input id="txtProductos" name="txtProductos" type="text" class="form-control input-sm" required="">
				
									 </div>   
				
								</div>
								<div class="col-lg-12">

									 <div class="form-group">
				
									  <label>Precio del producto </label>
				
									  <input id="txtProductos" name="txtProductos" type="text" class="form-control input-sm" required="">
				
									 </div>   
				
								</div>
							</div>
						
						</div>

                     </div>   

				</div>

				

			</div>

			

			

		

			 <div class="modal-footer" style="text-align:left">

            

            <button type="submit" class="btn btn-primary">Crear</button>
			<button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

         </div>

		</form>

         </div>

      </div>

   </div>

</div>


<div class="modal inmodal fade" id="myModalTipo" tabindex="-1" role="dialog"  aria-hidden="true">

   <div class="modal-dialog modal-lg">

      <div class="modal-content">

         <div class="modal-header" style="text-align:left">

            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

            <h4 class="modal-title">Crear/Editar Tipo</h4>

         </div>

         <div class="modal-body">

       	 <form class="m-t" role="form" action="#" required="" method="post">

             <div class="row" >

                 <div class="col-lg-12">

         	         <div class="form-group">

                      <label>Nombre </label>

                      <input id="txtNombre" name="txtNombre" type="text" class="form-control input-sm" required="">

                     </div>   

				</div>
				
				 <div class="col-lg-12">

         	         <div class="form-group">

                      <label>Posici&oacute;n </label>

                      <input id="txtPosicion" name="txtPosicion" type="text" class="form-control input-sm" required="">

                     </div>   

				</div>

				

			</div>

			

			

		

			 <div class="modal-footer" style="text-align:left; ">
 <button type="submit" class="btn btn-primary">Guardar</button>

            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

           
         </div>

		</form>

         </div>

      </div>

   </div>

</div>


<div class="modal inmodal fade" id="myModalCategoria" tabindex="-1" role="dialog"  aria-hidden="true">

   <div class="modal-dialog modal-lg">

      <div class="modal-content" >

         <div class="modal-header" style="text-align:left">

            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

            <h4 class="modal-title">Crear/Editar Categor&iacute;a</h4>

         </div>

         <div class="modal-body">

       	 <form class="m-t" role="form" action="mi_perfil.php" required="" method="post">

             <div class="row" >

                 <div class="col-lg-12">

         	         <div class="form-group">

                      <label>Nombre </label>

                      <input id="txtNombre" name="txtNombre" type="text" class="form-control input-sm" required="">

                     </div>   

				</div>
				
				 <div class="col-lg-12">

         	         <div class="form-group">

                      <label>Posici&oacute;n </label>

                      <input id="txtPosicion" name="txtPosicion" type="text" class="form-control input-sm" required="">

                     </div>   

				</div>

				

			</div>

			

			

		

			 <div class="modal-footer" style="text-align:left">
  <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

          

         </div>

		</form>

         </div>

      </div>

   </div>

</div>




<!-- FooTable -->
    <script src="js/plugins/footable/footable.all.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {

            $('.footable').footable();

           

        });

    </script>

<?php include_once "footer.inc.php"; ?>