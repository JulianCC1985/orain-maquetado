<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="google-signin-client_id" content="484963247159-o9bj6sk3oma2tdg84ighopps816hogfs.apps.googleusercontent.com">
      <title>Orain</title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
      <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
      <link href="css/animate.css" rel="stylesheet">
      <link href="css/style.css?v=2" rel="stylesheet">
      <script>
         window.onLoadCallback = function () {
            startApp();
         }
      </script>
      <script type="text/javascript" src="https://apis.google.com/js/platform.js?onload=onLoadCallback" async defer></script>
      <script type="text/javascript" src="js/google.js"></script>
	   <style>
	 body{
	 	background-image:url(img/Fondo_login.jpg);
		-webkit-background-size: cover; 
		-moz-background-size: cover; 
		-o-background-size: cover; 
		background-size: cover;
	 }
	 </style>
   </head>
   <body class="gray-bg">
      <script>
         window.fbAsyncInit = function() {
           FB.init({
             appId            : '1664993556858878',
             autoLogAppEvents : true,
             xfbml            : true,
             version          : 'v2.10'
           });
           FB.AppEvents.logPageView();
         };
         
         (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
      </script>
      <div class="middle-box text-center loginscreen   animated fadeInDown">
      <div style="margin-top:120px">
          <div class="ibox-content">
         <h2>Crea una cuenta.</h2>
         <form class="m-t" role="form" action="index.php" required="">
            <div class="tabs-container ">
               <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#tab-1"> Usuario</a></li>
                  <li class=""><a data-toggle="tab" href="#tab-2">Empresa</a></li>
               </ul>
               <div class="tab-content ">
                  <div id="tab-1" class="tab-pane active ">
                     <div class="panel-body">
                        <div class="form-group">
                           <input type="email" class="form-control" placeholder="Email" required="">
                        </div>
                        <div class="form-group">
                           <input type="password" class="form-control" placeholder="Contrase&ntilde;a" required="">
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>
                        <button class="btn btn-block btn-info m-t-15 " type="button" style="background-color:#39579b; border:solid 1px #39579b" onClick="FB.login(function (response) { checkLoginState(); }, {scope: 'public_profile,email'});">
                        <span class="pull-left">
                        <i class="fa fa-facebook"></i>
                        </span>
                        <span class="bold">Registrate con Facebook</span>
                        </button>
                        <button class="btn btn-danger block full-width m-b" type="button" id="googlesignin" style="margin-top:13px" >
                        <span class="pull-left">
                        <i class="fa fa-google-plus"></i>
                        </span>
                        <span class="bold">Registrate con Google+</span>
                        </button>
                        <div align="center">
                           <a href="forgot_password.php">
                           <small>Olvidaste tu contrase&ntilde;a?</small>
                           </a>
                        </div>
                        <p class="text-muted text-center">
                           <small>No tienes una cuenta?</small>
                        </p>
                        <a class="btn btn-sm btn-white btn-block" href="register.php">Crea una cuenta</a>
                     </div>
                  </div>
                  <div id="tab-2" class="tab-pane">
                     <div class="panel-body">
                        <div class="form-group">
                           <input type="email" class="form-control" placeholder="Email" required="">
                        </div>
                        <div class="form-group">
                           <input type="password" class="form-control" placeholder="Contrase&ntilde;a" required="">
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>
                       
                        <div align="center">
                           <a href="forgot_password.php">
                           <small>Olvidaste tu contrase&ntilde;a?</small>
                           </a>
                        </div>
                        <p class="text-muted text-center">
                           <small>No tienes una cuenta?</small>
                        </p>
                        <a class="btn btn-sm btn-white btn-block" href="register.php">Crea una cuenta</a>
                     </div>
                  </div>
               </div>
         </form>
         </div>
		 </div>
      </div>
      <!-- Mainly scripts -->
      <script src="js/jquery-3.1.1.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <!-- iCheck -->
      <script src="js/plugins/iCheck/icheck.min.js"></script>
      <script>
         $(document).ready(function(){
             $('.i-checks').iCheck({
                 checkboxClass: 'icheckbox_square-green',
                 radioClass: 'iradio_square-green',
             });
         });
      </script>
   </body>
</html>