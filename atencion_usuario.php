<?php include_once "header.inc.php"; ?>
 <div class="row wrapper border-bottom page-heading">
                <div class="col-lg-10">
                    <h2><i class="fa fa-comments"></i> Atenci&oacute;n al usuario</h2>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
 <div class="wrapper wrapper-content">
 	<div class="row">
			 <div class="col-lg-10">
			    <div class="mail-box-header">
							<form method="get" action="index.html" class="pull-right mail-search">
							<div class="input-group">
								<input type="text" class="form-control input-sm" name="search" placeholder="Ingrese la busqueda">
								<div class="input-group-btn">
									<button type="submit" class="btn btn-sm btn-primary">
										Buscar
									</button>
								</div>
							</div>
						</form>
						<h2>
							Bandeja de entrada
						</h2>
						<div class="mail-tools">
							<div class="btn-group pull-right">
								<button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
								<button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>
		
							</div>
							<button class="btn btn-white btn-sm"><i class="fa fa-refresh"></i> Actualizar</button>
							<button class="btn btn-white btn-sm" title="Marcar como leido"><i class="fa fa-eye"></i> </button>
							<button class="btn btn-white btn-sm" title="Eliminar"><i class="fa fa-trash-o"></i> </button>
		
						</div>
           		 </div>
				  <div class="mail-box">
				  
				  <table class="table table-hover table-mail">
				  		<thead>
							<th></th>
							<th>Fecha</th>
							<th>Estado</th>
							<th>Usuario</th>
							<TH>M&aacute;quina/Sector</TH>
							<th>Tipolog&iacute;a mensaje</th>
							<th>Mensaje</th>
							<th></th>
						</thead>
						<tbody>
						<tr class="unread">
							<td class="check-mail">
								<input type="checkbox" class="i-checks">
							</td>
							<td>17/05/2017</td>
							<td>Recibido</td>
							
							<td>Anna Smith</td>
							<td>M&aacute;quina</td>
							<td>Tipo</td>
							<td class="mail-subject">Lorem ipsum dolor noretek imit set.</td>
							<td><i class="fa fa-reply" aria-hidden="true" data-toggle="modal" data-target="#myModal5"  style="cursor:pointer"></i></td>
						
						</tr>
						<tr class="read" >
							<td class="check-mail">
								<input type="checkbox" class="i-checks">
							</td>
							<td>17/05/2017</td>
							<td>Recibido</td>
							
							<td>Jack Nowak</td>
							<td>M&aacute;quina</td>
							<td>Tipo</td>
							<td class="mail-subject">Aldus PageMaker including versions of Lorem Ipsum.</td>
							<td><i class="fa fa-reply" aria-hidden="true" data-toggle="modal" data-target="#myModal5" style="cursor:pointer"></i></td>
						</tr>
						
					</tbody>
				  </table>
				  
				  </div>
            </div>
     

           
			<div class="col-lg-2">
				<div class="widget style1 navy-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Pendientes </span>
                            <h2 class="font-bold">100</h2>
                        </div>
                    </div>
                </div>
			
				<h3 class="font-bold">Tipolog&iacute;a de mensajes</h3>
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
				
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
				
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
				
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
			
			</div>
</div>
</div>

<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title">Responder mensaje</h4>
                                            <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                                        </div>
                                        <div class="modal-body">
                                            
											<div class="feed-activity-list" style="border-bottom:none">
												<div class="feed-element" style="border-bottom:none">
													<a href="#" class="pull-left">
														<img alt="image" class="img-circle" src="img/a2.jpg">
													</a>
													<div class="media-body ">
														<small class="pull-right">2h ago</small>
														<strong>Mark Johnson</strong> posted message on <strong>Monica Smith</strong> site. <br>
														<small class="text-muted">Today 2:10 pm - 12.06.2014</small>
														<div class="well">
															Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
															Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
														</div>
														<div>
															<textarea class="form-control" style="resize: none" rows="5"></textarea>
														</div>
													</div>
												</div>
											</div>
											
											
											
											
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <button type="button" class="btn btn-primary">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


<script src="js/plugins/dataTables/datatables.min.js?v=2.33"></script>
<script>
function MuestraRespuesta(id){
	$("#linea_"+id).show();
}

$(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ]

            });

        });
</script>
 <script src="js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
<?php include_once "footer.inc.php"; ?>