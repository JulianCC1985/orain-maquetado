<?php include_once "header.inc.php"; ?>
<style>
.precio{
	color:#FF0000; font-weight:bold; font-size:18px;
	text-align:center
}
.linea {
	padding:10px;
	border-top:solid 1px #ccc
}
</style>
<div class="row wrapper border-bottom page-heading">
   <div class="col-lg-10">
      <h2><i class="fa fa-sticky-note-o"></i> Facturaci&oacute;n</h2>
   </div>
   <div class="col-lg-2">
   </div>
</div>
<div class="wrapper wrapper-content">

<div class="tabs-container">
    <ul class="nav nav-tabs">
         <li class="active"><a data-toggle="tab" href="#tab-1"> Tarifa</a></li>
         <li class=""><a data-toggle="tab" href="#tab-2">TPV</a></li>
		  <li class=""><a data-toggle="tab" href="#tab-3">Cuentas corrientes</a></li>
		   <li class=""><a data-toggle="tab" href="#tab-4">Mis facturas</a></li>
	</ul>
	
		<div class="tab-content">
            <div id="tab-1" class="tab-pane active">
				<div class="row">
					<div class="col-lg-12">
							 <div class="ibox">
							  <div class="ibox-content">
								 <div class="row">
								 
									 
						

            <div class="col-lg-4">
                <ul class="landing-page pricing-plan list-unstyled" id="plan_1">
                    <li class="landing-page pricing-title" style="background-color:#999999; text-align:center">
                         Orain B&aacute;sico
                    </li>
                    <li class="landing-page pricing-price" style="text-align:center; padding:10px">
                        <span class="precio">3,5&euro; maq/mes<br />+IVA</span> 
                    </li>
                    <li class="linea">
                       Sistema de pago a trav&eacute;s del m&oacute;vil
                    </li>
                    <li class="linea">
                        Promoci&oacute;n descarga
                    </li>
                    <li class="linea">
                        Datos b&aacute;sicos
                    </li>
                    <li class="linea">
                        Centro de resoluci&oacute;n de incidencias
                    </li>
                    <li style="text-align:center" class="linea">
                        <a class="btn btn-primary btn-xs" href="javascript:MarcarPlan(1)" id="hrefPlan_1">Seleccionar</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-4">
                <ul class="landing-page pricing-plan list-unstyled" id="plan_2">
                    <li class="landing-page pricing-title" style="background-color:#999999; text-align:center; ">
                         Orain Datos
                    </li>
                    <li class="landing-page pricing-price" style="text-align:center; padding:10px">
                        <span class="precio">4,5&euro; maq/mes<br />+IVA</span> 
                    </li>
                    <li class="linea">
                       Venta por m&aacute;quinas

                    </li>
                    <li class="linea">
                        Venta por usuarios
                    </li>
                    <li class="linea">
                       Venta por productos
                    </li>
                    <li style="text-align:center" class="linea">
                        <a class="btn btn-primary btn-xs" href="javascript:MarcarPlan(2)" id="hrefPlan_2">Seleccionar</a>
                    </li>
                </ul>
            </div>

            <div class="col-lg-4">
               <ul class="landing-page pricing-plan list-unstyled" id="plan_3">
                    <li class="landing-page pricing-title" style="background-color:#999999; text-align:center">
                         B.I. Datos
                    </li>
                    <li class="landing-page pricing-price" style="text-align:center; padding:10px">
                        <span class="precio">1,5&euro; maq/mes<br />+IVA</span> 
                    </li>
                    <li class="linea">
                       Optimizaci&oacute;n parque de m&aacute;quinas
                    </li>
                    <li class="linea">
                        Informe personalizado 
                    </li>
                    <li style="text-align:center" class="linea">
                        <a class="btn btn-primary btn-xs" href="javascript:MarcarPlan(3)" id="hrefPlan_3">Seleccionar</a>
                    </li>
                </ul>
            </div>
  


								 
								 
								 
								 
								 
								 
								 
								 
								 
								 </div>
								 
								 <div class="row">
								 	
									<div class="col-lg-4" >
										<ul class="landing-page pricing-plan list-unstyled" id="plan_4">
											<li class="landing-page pricing-title" style="background-color:#999999; text-align:center">
												 Orain Comunicaci&oacute;n
											</li>
											<li class="landing-page pricing-price" style="text-align:center; padding:10px">
												<span class="precio">2,5&euro; maq/mes<br />+IVA</span> 
											</li>
											<li class="linea">
											   Gestor de promociones
											</li>
											<li class="linea">
												Sistema de notificaciones (50 mensajes m&aacute;quina/mes)
											</li>
											<li class="linea">
												Respuestas inteligentes
											</li>
											<li class="linea">
												Centro de resoluci&oacute;n de incidencias
											</li>
											<li style="text-align:center" class="linea">
												<a class="btn btn-primary btn-xs" href="javascript:MarcarPlan(4)" id="hrefPlan_4">Seleccionar</a>
											</li>
										</ul>
									</div>
									
									
									<div class="col-lg-4">
										<ul class="landing-page pricing-plan list-unstyled" id="plan_5">
											<li class="landing-page pricing-title" style="background-color:#999999; text-align:center">
												 B.I. Comunicaci&oacute;n
											</li>
											<li class="landing-page pricing-price" style="text-align:center; padding:10px">
												<span class="precio">1&euro; maq/mes<br />+IVA</span> 
											</li>
											<li class="linea">
											   Clasificaci&oacute;n de mensajes
											</li>
											<li class="linea">
												Recomendaci&oacute;n promociones
											</li>
											<li style="text-align:center" class="linea">
												<a class="btn btn-primary btn-xs" href="javascript:MarcarPlan(5)" id="hrefPlan_5">Seleccionar</a>
											</li>
										</ul>
									</div>
									
								  </div>
								  
								  
								 
								 
								 
							</div>
						</div>
					</div>
				</div>
				
			</div>
			 <div id="tab-2" class="tab-pane">
			 	<div class="row">
					<div class="col-lg-12">
							 <div class="ibox">
							  <div class="ibox-content">
								 <div class="row">aaa
								 </div>
							</div>
						</div>
					</div>
				</div>
			 
			</div>
			<div id="tab-3" class="tab-pane">
				<div class="row">
					<div class="col-lg-12">
							 <div class="ibox">
							  <div class="ibox-content">
								 <div class="row">aaa
								 </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="tab-4" class="tab-pane">
				<div class="row">
					<div class="col-lg-12">
							 <div class="ibox">
							  <div class="ibox-content">
								 <div class="row">aaa
								 </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
<script>
function MarcarPlan(id){
	if(document.getElementById("hrefPlan_"+id).innerHTML == "Quitar"){
		document.getElementById("plan_"+id).style.backgroundColor = "#ffffff"; 
		document.getElementById("hrefPlan_"+id).innerHTML = "Seleccionar";
	}
	else{
		document.getElementById("plan_"+id).style.backgroundColor = "#eeeeee"; 
		document.getElementById("hrefPlan_"+id).innerHTML = "Quitar";
	}
}
</script>
<?php include_once "footer.inc.php"; ?>