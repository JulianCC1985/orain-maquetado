<?php include_once "header.inc.php"; ?>
<div class="row wrapper border-bottom page-heading">
     <div class="col-lg-10">
          <h2><i class="fa fa-comments"></i> Comunicaci&oacute;n / Gestor de Promociones / Crear campa&ntilde;a</h2>
     </div>
     <div class="col-lg-2">

                </div>
</div>



<div class="wrapper wrapper-content animated fadeInRight">
            
           
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Asistente para la creaci&oacute;n de campa&ntilde;as</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                              
                            </div>
                        </div>
                        <div class="ibox-content">

                            <form id="form" action="#" class="wizard-big">
                                <h1>Destinatarios</h1>
                                <fieldset>
									 <h2>Destinatarios</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                           Los usuarios deben coincider con 
										   <select class="input-sm">
										   		<option value="1">Todas</option>
												<option value="2">Algunas</option>
										   </select> de las siguientes condiciones:
                                            
                                        </div>
                                    </div>
									
									 <div class="row" style="margin-top:15px">
									<div class="form-group col-lg-12">
                                            <label>Fecha de conexi&oacute;n con la m&aacute;quina </label>
											<div id="reportrange" class="form-control">
												<i class="fa fa-calendar"></i>
												<span></span> <b class="caret"></b>
											</div>

                                        </div>
									</div>
									
									 <div class="row">
										<div class="col-lg-12">
										<label>Seleccion de usuarios </label>
										<label id="cmbMaquinas-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbMaquinas[]" id="cmbMaquinas" class="form-control dual_select required" multiple style=" background-color:#0000CC">
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>
									
									 <div class="row" style="margin-top:10px">
										<div class="col-lg-12">
										<label>Seleccion de ubicaciones </label>
										<label id="cmbMaquinas-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbMaquinas[]" id="cmbMaquinas" class="form-control dual_select required" multiple style=" background-color:#0000CC">
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>
									
									 <div class="row" style="margin-top:10px">
										<div class="col-lg-12">
										<label>Seleccion de maquinas </label>
										<label id="cmbMaquinas-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbMaquinas[]" id="cmbMaquinas" class="form-control dual_select required" multiple style=" background-color:#0000CC">
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>
									
									 <div class="row" style="margin-top:10px">
										<div class="col-lg-12">
										<label>Grupo de usuarios </label>
										<label id="cmbMaquinas-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbMaquinas[]" id="cmbMaquinas" class="form-control dual_select required" multiple style=" background-color:#0000CC">
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>
									
									 <div class="row" style="margin-top:10px">
										<div class="col-lg-12">
										<label>Idioma </label>
										<label id="cmbMaquinas-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbMaquinas[]" id="cmbMaquinas" class="form-control dual_select required" multiple style=" background-color:#0000CC">
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>

                                </fieldset>
                                <h1>Contenido</h1>
                                <fieldset>
                                    <h2>Contenido</h2>
									<div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Nombre de la campa&ntilde;a </label>
                                                <input id="txtNombreCampania" name="txtNombreCampania" type="text" class="form-control input-sm required">
                                            </div>
											
											  <div class="form-group">
                                                <label>Contenido (m&aacute;x. 140 car&aacute;cteres)</label>
                                               <textarea class="form-control input-sm required" rows="6" style="resize: none" maxlength="140"></textarea>
											   <label id="lbTotCaracteres"></label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                                <h1>Confirmaci&oacute;n</h1>
                                <fieldset>
									 <h2>Confirmaci&oacute;n</h2>
									 <DIV class="row">
									  <div class="col-lg-12">
                                     		<div class="form-group">
												<div class="row">
													<div class="col-lg-9">
														<div class="i-checks"><label> <input type="radio" checked="checked" id="chkPaso_1" value="1" name="chkPaso_1"> <i></i> <span style="font-weight:normal">Nombre de la campa&ntilde;a. </span> </label></div>
													</div>
													<div class="col-lg-3">
														<button type="button" class="btn btn-white">Editar</button>
													</div>
												 </div>
												 
												 
												 <div class="row" style="margin-top:15px">
													<div class="col-lg-9">
														<div class="i-checks"><label> <input type="radio" checked="checked" id="chkPaso_2" value="1" name="chkPaso_2"> <i></i> <span style="font-weight:normal">N&uacute;mero de usuarios que reciben la campa&ntilde;a.</span> </label></div>
													</div>
													<div class="col-lg-3">
														<button type="button" class="btn btn-white">Editar</button>
													</div>
												 </div>
												 
												  <div class="row" style="margin-top:15px">
													<div class="col-lg-9">
														<div class="i-checks"><label> <input type="radio" checked="checked" id="chkPaso_3" value="1" name="chkPaso_3"> <i></i> <span style="font-weight:normal">Contenido de la campa&ntilde;a.</span> </label></div>
													</div>
													<div class="col-lg-3">
														<button type="button" class="btn btn-white">Editar</button>
													</div>
												 </div>
												 
												  <div class="row" style="margin-top:15px">
													<div class="col-lg-9">
														<div class="i-checks"><label> <input type="radio" checked="checked" id="chkPaso_4" value="1" name="chkPaso_4"> <i></i> <span style="font-weight:normal">Calendario y reloj para programar la campa&ntilde;a.</span> </label></div>
													</div>
													<div class="col-lg-3">
														<button type="button" class="btn btn-white">Editar</button>
													</div>
												 </div>
												 
												 
                                            </div>

										</div>
										
									</DIV>
                                </fieldset>

                              
                            </form>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
<script src="js/plugins/steps/jquery.steps.min.js?v=2"></script>
<!-- Jquery Validate -->
<script src="js/plugins/validate/jquery.validate.min.js?v=2"></script>

  <script>
  
  
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
					
                    // Always allow going backward even if the current step contains invalid fields!
					if(currentIndex == 0){
						  $("textarea[maxlength]").keyup(function() {
						         var limit   = $(this).attr("maxlength"); // L�mite del textarea
						         var value   = $(this).val();             // Valor actual del textarea
						         var current = value.length;              // N�mero de caracteres actual
								 current = (limit - current);
								 $("#lbTotCaracteres").html("Car&aacute;cteres restantes: "+current);
						         /*if (limit < current) {                   // M�s del l�mite de caracteres?
						             // Establece el valor del textarea al l�mite
						             $(this).val(value.substring(0, limit));
						         }*/
						     });
					}
					
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
				
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }
                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";
					
                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
					
					$("#chkDescuento_1").click(function(){ alert("you are here");});
					
       });
    </script>
    <script src="js/plugins/iCheck/icheck.min.js"></script>
	<script>
            $(document).ready(function () {
				 $('.chosen-select').chosen({width: "100%"});
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
				
				$('.dual_select').bootstrapDualListbox({
                	selectorMinimalHeight: 160
            	});
				
				
				$('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#reportrange').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: { days: 60 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '�ltimos 7 d�as': [moment().subtract(6, 'days'), moment()],
                    '�ltimos 30 d�as': [moment().subtract(29, 'days'), moment()],
                    'Este mes': [moment().startOf('month'), moment().endOf('month')],
                    'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Aplicar',
                    cancelLabel: 'Cancelar',
                    fromLabel: 'Desde',
                    toLabel: 'hasta',
                    customRangeLabel: 'Personalizar',
                    daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    firstDay: 1
                }
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });
				
            });
        </script>
	 <script src="js/plugins/dualListbox/jquery.bootstrap-duallistbox.js?v=21"></script>

<?php include_once "footer.inc.php"; ?>