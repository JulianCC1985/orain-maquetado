<?php include_once "header.inc.php"; ?>
 <div class="row wrapper border-bottom page-heading">
                <div class="col-lg-10">
                    <h2><i class="fa fa-comments"></i> Atenci&oacute;n al usuario</h2>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
 <div class="wrapper wrapper-content">
 	<div class="row">
			<div class="col-lg-9">
			
				<div class="mail-box-header">
					<div class="pull-right ">
						<a href="atencion_usuario_respuesta.php" class="btn btn-white btn-sm" title="Responder"><i class="fa fa-reply"></i> Responder</a>
						<a href="atencion_usuario.php" class="btn btn-white btn-sm" title="Ir a la bandeja de entrada"><i class="fa fa-inbox"></i> </a>
						<a href="#" class="btn btn-white btn-sm" title="Imprimir"><i class="fa fa-print"></i> </a>
						<a href="mailbox.html" class="btn btn-white btn-sm" title="Eliminar"><i class="fa fa-trash-o"></i> </a>
					</div>
					<h2>
						T&iacute;tulo del mensaje
					</h2>
              		<div class="mail-tools tooltip-demo m-t-md">
						<h3>
							<span class="font-normal">Asunto: </span>Aldus PageMaker including versions of Lorem Ipsum.
						</h3>
						<h5>
							<span class="pull-right font-normal">10:15AM 02 FEB 2014</span>
							<span class="font-normal">De: </span>alex.smith@corporation.com
						</h5>
					</div>
				</div>
				
				
				<div class="mail-box">


					<div class="mail-body">
						<p>
							Hello Jonathan!
							<br/>
							<br/>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
							took a galley of type and scrambled it to make a type <strong>specimen book.</strong>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. It has survived not only five centuries, but also the leap into electronic typesetting, remaining
							essentially unchanged.
							</p>
						<p>
							It was popularised in the 1960s with the release <a href="#" class="text-navy">Letraset sheets</a>  containing Lorem Ipsum passages, and more recently with desktop publishing software
							like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
						<p>
							There are many variations of passages of <strong>Lorem Ipsum</strong>Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of.
						</p>
					</div>
					
					<div class="mail-body text-right tooltip-demo">
                                <a class="btn btn-sm btn-white" href="atencion_usuario_respuesta.php"><i class="fa fa-reply"></i> Responder</a>
                                <button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="Imprimir" class="btn btn-sm btn-white"><i class="fa fa-print"></i> Imprimir</button>
                                <button title="" data-placement="top" data-toggle="tooltip" data-original-title="Eliminar" class="btn btn-sm btn-white"><i class="fa fa-trash-o"></i> Eliminar</button>
                        </div>
				</div>
				
				
			
			
		    </div>

           
			<div class="col-lg-3">
				<div class="widget style1 navy-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Pendientes </span>
                            <h2 class="font-bold">100</h2>
                        </div>
                    </div>
                </div>
			
				<h3 class="font-bold">Tipolog&iacute;a de mensajes</h3>
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
				
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
				
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
				
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
			
			</div>
          
  
</div>

</div>


<script src="js/plugins/dataTables/datatables.min.js?v=2.33"></script>
<script>
function MuestraRespuesta(id){
	$("#linea_"+id).show();
}

$(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ]

            });

        });
</script>
 <script src="js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
<?php include_once "footer.inc.php"; ?>