<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Orain</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css?v=2" rel="stylesheet">
	 <style>
	 body{
	 	background-image:url(img/Fondo_login.jpg);
		-webkit-background-size: cover; 
		-moz-background-size: cover; 
		-o-background-size: cover; 
		background-size: cover;
	 }
	 </style>

</head>

<body class="gray-bg">

    <div class="passwordBox animated fadeInDown">
		
       <div class="row" style="margin-top:100px">

            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">&iquest;Olvidaste tu contrase&ntilde;a?</h2>

                    <p>
                        Ingresa tu email y te seran enviadas las instrucciones para reestrablecer tu contrase&ntilde;a.
                    </p>

                    <div class="row">

                        <div class="col-lg-12" align="center">
                            <form class="m-t" role="form" action="index.html">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email" required="">
                                </div>
						  <div class="col-lg-12" align="center" style="margin-bottom:20px">
								 <a href="login.php">
                            <small>Ya tienes cuenta? Ingresa</small>
                        </a>
						</div>

                                <button type="submit" class="btn btn-primary block full-width m-b">Enviar</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
		
    </div>

</body>

</html>
