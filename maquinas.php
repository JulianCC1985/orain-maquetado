<?php include_once "header.inc.php"; ?>

<div class="row wrapper border-bottom page-heading">

   <div class="col-lg-10">

      <h2><i class="fa fa-cog"></i> Configuraci&oacute;n / M&aacute;quinas</h2>

   </div>

   <div class="col-lg-2">

   </div>

</div>

<div class="wrapper wrapper-content">

<div class="row">

<div class="col-md-12">

   <div class="ibox-content">

      <div class="tabs-container">

         <ul class="nav nav-tabs">

            <li class="active"><a data-toggle="tab" href="#tab-1"> Vending</a></li>

            <li class=""><a data-toggle="tab" href="#tab-2">OCS</a></li>

         </ul>

         <div class="tab-content">

            <div id="tab-1" class="tab-pane active">

			<div class="row">

			   		 <div class="row" style="margin-left:0px; margin-top:15px">

                              <div class="form-group col-lg-2" >

								
                                 <button type="submit" class="btn btn-primary block full-width m-b btn-lg" onclick="location.href='maquinas_admin_vending.php'"><i class="fa fa-plus" aria-hidden="true"></i> A&ntilde;adir m&aacute;quina</button>
								 
                              </div>

                     </div>

					 

					  <div class="col-lg-12">

						 <div class="ibox float-e-margins">

							<div class="table-responsive">

							 <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">


								  <thead>

									 <tr>

										<th>N&uacute;mero</th>

										<th>Nombre</th>
										<th>Etiqueta de ubicaci&oacute;n</th>
										<th>Tipo</th>
										<th>C&oacute;digo de activaci&oacute;n</th>
										<th>Moneda</th>
										<th>Decimales</th>
										<th>Factor de moneda</th>
										<th>Time out</th>

										<th style="text-align:center">Opciones</th>

									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=10; $i++){ ?>

									 <tr>

										<td>1</td>

										<td>M&aacute;quina en vending </td>
										<td>Etiqueta</td>
										<td>Tipo</td>
										<td>C&oacute;digo</td>
										<td>Moneda</td>
										<td>Decimales</td>
										<td>Factor</td>
										<td>Timeout</td>

										<td class="center" style="text-align:center">
										<span class="label label-primary" style="cursor:pointer" onclick="location.href='maquinas_admin_vending.php'">Editar</span>
										</td>

									 </tr>

									 <?php }?>

								  </tbody>
								   <tfoot>
<tr>
                                    <td colspan="10">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
								</tfoot>
							   </table>

							</div>

							

						 </div>

                  	</div>

				</div>



				

			

			

			</div>

			<div id="tab-2" class="tab-pane">

			

				<div class="row">

			   		 <div class="row" style="margin-left:0px; margin-top:15px">

                              <div class="form-group col-lg-2" >

                                  <button type="submit" class="btn btn-primary block full-width m-b btn-lg" onclick="location.href = 'maquinas_admin_ocs.php'"><i class="fa fa-plus" aria-hidden="true"></i> A&ntilde;adir m&aacute;quina</button>
                              </div>

                     </div>

					 

					  <div class="col-lg-12">

						 <div class="ibox float-e-margins">

							<div class="table-responsive">

							 <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">


								  <thead>

									 <tr>

										<th>No.</th>

										<th>M&aacute;quina</th>

										<th style="text-align:center">Opciones</th>

									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=10; $i++){ ?>

									 <tr>

										<td>1</td>

										<td>Maquina en OCS </td>

										<td class="center" style="text-align:center">
										<span class="label label-primary" style="cursor:pointer" onclick="location.href = 'maquinas_admin_ocs.php'">Editar</span>
										</td>

									 </tr>

									 <?php }?>

								  </tbody>
								   <tfoot>
									<tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
								</tfoot>
							   </table>

							</div>

							

						 </div>

                  	</div>

				</div>

			

			

			</div>

		 </div>

	 </div>

	</div>

</div>



<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">

   <div class="modal-dialog modal-lg">

      <div class="modal-content">

         <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

            <h4 class="modal-title">Registrar / Editar Maquina</h4>

         </div>

         <div class="modal-body">

       	 <form class="m-t" role="form" action="mi_perfil.php" required="" method="post">

             <div class="row" >

                 <div class="col-lg-12">

         	         <div class="form-group">

                      <label>Nombre </label>

                      <input id="txtNombre" name="txtNombre" type="text" class="form-control input-sm" required="">

                     </div>   

				</div>

				

			</div>

			

			

		

			 <div class="modal-footer">

            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

            <button type="submit" class="btn btn-primary">Guardar</button>

         </div>

		</form>

         </div>

      </div>

   </div>

</div>

<!-- FooTable -->
    <script src="js/plugins/footable/footable.all.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {

            $('.footable').footable();

           

        });

    </script>

<?php include_once "footer.inc.php"; ?>