var googleUser = {};
var startApp = function () {
    gapi.load('auth2', function () {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi.auth2.init({
            client_id: '484963247159-o9bj6sk3oma2tdg84ighopps816hogfs.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        });
        attachSignin(document.getElementById('googlesignin'));
    });
};

function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function (googleUser) {
            onSignIn(googleUser);
        }, function (error) {
        });
}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    data = {};
    data.id_token = googleUser.getAuthResponse().id_token;
    data.name = profile.getName();
    data.picture = profile.getImageUrl();
    data.email = profile.getEmail();
	alert(data.email);
	location.href = 'index.php';

};

function renderButton() {
    gapi.signin2.render('my-signin2', {
        'scope': 'https://www.googleapis.com/auth/plus.login',
        'width': 200,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
    });
}