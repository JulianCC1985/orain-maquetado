<?php 

$res = explode("/", $_SERVER['PHP_SELF']);

$url = $res[count($res)-1];

?>



<div class="sidebar-collapse">

            <ul class="nav metismenu" id="side-menu">

                <li class="nav-header">

                    <div class="dropdown profile-element"> <span>

                            <img alt="image" class="img-circle" src="img/profile_small.jpg" />

                             </span>

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>

                             </span> <span class="text-muted text-xs block">Nombre de la empresa <b class="caret"></b></span> </span> </a>

                        <ul class="dropdown-menu animated fadeInRight m-t-xs">

                            <li><a href="mi_perfil.php">Mi Perfil/Administraci&oacute;n</a></li>

                            <li><a href="facturacion.php">Facturaci&oacute;n</a></li>

                            <li class="divider"></li>

                            <li><a href="login.php">Salir</a></li>

                        </ul>

                    </div>

                    <div class="logo-element">

                        IN+

                    </div>

                </li>

				  <li <?php if($url == 'index.php') {?> class="active" <?php }?>>

                    <a href="index.php"><i class="fa fa-th-large"></i> <span class="nav-label">Principal</span></a>

                </li>

                <li  <?php if($url == 'mi_negocio.php' || $url == 'usuarios.php' || $url == 'estadisticas.php' || $url == 'business.php') {?> class="active" <?php }?> >

                    <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Datos</span> <span class="fa arrow"></span></a>

                    <ul class="nav nav-second-level">

                        <li <?php if($url == 'mi_negocio.php') {?> class="active" <?php }?>><a href="mi_negocio.php">Mi negocio</a></li>

                        <li <?php if($url == 'usuarios.php') {?> class="active" <?php }?>><a href="usuarios.php">Usuarios</a></li>

						<li <?php if($url == 'estadisticas.php') {?> class="active" <?php }?>><a href="estadisticas.php">Estad&iacute;sticas</a></li>

						<li <?php if($url == 'bussiness.php') {?> class="active" <?php }?>><a href="bussiness.php">Business Intelligence</a></li>

                    </ul>

                </li>

                <li <?php if($url == 'promociones.php' || $url == 'crear_promocion.php' || $url == 'atencion_usuario.php' || $url == 'crear_campania.php') {?> class="active" <?php }?>>

                     <a href="index.html"><i class="fa fa-comments"></i> <span class="nav-label">Comunicaci&oacute;n</span> <span class="fa arrow"></span></a>

                    <ul class="nav nav-second-level">

                        <li <?php if($url == 'atencion_usuario.php') {?> class="active" <?php }?>><a href="atencion_usuario.php">Atenci&oacute;n al usuario</a></li>

                        <li <?php if($url == 'promociones.php' || $url == 'crear_promocion.php' || $url == 'crear_campania.php') {?> class="active" <?php }?>><a href="promociones.php">Gestor de promociones</a></li>

						<li><a href="#">Personalizaci&oacute;n</a></li>

						<li><a href="#">Business Intelligence</a></li>

                    </ul>

                </li>

				  <li <?php if($url == 'ecommerce.php') {?> class="active" <?php }?>>

                    <a href="ecommerce.php"><i class="fa fa-shopping-cart"></i> <span class="nav-label">E-commerce</span></a>

                </li>
				

				<li <?php if($url == 'maquinas.php' || $url == 'productos.php' || $url == 'categorias.php' || $url == 'tipos.php' || $url == 'grupos.php' || $url == 'maquinas_admin.php') {?> class="active" <?php }?>>

                    <a href=""><i class="fa fa-cog"></i> <span class="nav-label">Configuraci&oacute;n</span></a>

					<ul class="nav nav-second-level">

                        <li <?php if($url == 'maquinas.php' || $url == 'maquinas_admin.php') {?> class="active" <?php }?>><a href="maquinas.php">M&aacute;quinas</a></li>

						<li <?php if($url == 'productos.php') {?> class="active" <?php }?>><a href="productos.php">Productos</a></li>


					</ul>

                </li>



            </ul>



        </div>