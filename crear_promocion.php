<?php include_once "header.inc.php"; ?>
<div class="row wrapper border-bottom page-heading">
     <div class="col-lg-10">
          <h2><i class="fa fa-comments"></i> Comunicaci&oacute;n / Gestor de Promociones / Crear promociones</h2>
     </div>
     <div class="col-lg-2">

                </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Asistente para la creaci&oacute;n de promociones</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                              
                            </div>
                        </div>
                        <div class="ibox-content">

                            <form id="form" action="#" class="wizard-big">
                                <h1>Informaci&oacute;n de la promoci&oacute;n</h1>
                                <fieldset>
									 <h2>Informaci&oacute;n de la promoci&oacute;n</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Nombre de la promoci&oacute;n </label>
                                                <input id="txtNombrePromocion" name="txtNombrePromocion" type="text" class="form-control input-sm required">
                                            </div>
                                            <div class="form-group">
                                                <label>Descripci&oacute;n de la promoci&oacute;n </label>
												<input id="txtDescripcion" name="txtDescripcion" type="text" class="form-control input-sm required">
                                               
                                            </div>
                                            <div class="form-group">
                                                <label>C&oacute;digo </label>
                                                <input id="txtCodigo" name="txtCodigo" type="text" class="form-control input-sm required">
                                            </div>
											<div class="form-group">
                                                <label>Estado </label>
												<div class="row">
													<div class="col-lg-3">
														<div class="i-checks"><label> <input type="radio" checked="" value="option2" name="a"> <i></i> <span style="font-weight:normal">Activar </span></label></div>
													</div>
													<div class="col-lg-3">
													 	<div class="i-checks"><label> <input type="radio" value="option2" name="a"> <i></i> <span style="font-weight:normal">Deseactivar</span> </label></div>
													</div>
												 </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                <h1>Condiciones de la promoci&oacute;n</h1>
                                <fieldset>
                                    <h2>Condiciones de la promoci&oacute;n</h2>
									<div class="row">
										<div class="form-group col-lg-4">
                                            <label>Cliente </label>
                                            <select data-placeholder="Choose a Country..." name="cmbCliente" id="cmbCliente" class="form-control input-sm chosen-select"  >
												<option value="">Select</option>
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Afghanistan">Afghanistan</option>
												<option value="Aland Islands">Aland Islands</option>
											</select>
                                        </div>
										<div class="form-group col-lg-5">
                                            <label>Validez </label>
											<div id="reportrange" class="form-control">
												<i class="fa fa-calendar"></i>
												<span></span> <b class="caret"></b>
											</div>

                                        </div>
										<div class="form-group col-lg-3">
                                            <label>Gasto m&iacute;nimo &euro; </label>
                                          <input id="txtGasto" name="txtGasto" type="text" class="form-control input-sm required">
                                        </div>
									</div>
									<div class="row">
										<div class="form-group col-lg-4">
                                            <label>Total disponible </label>
                                            <input id="txtTotalDisponible" name="txtTotalDisponible" type="text" class="form-control input-sm required" />
                                        </div>
										<div class="form-group col-lg-4">
                                            <label>Total disponible para cada usuario </label>
                                            <input id="txtDisponibleUsuario" name="txtDisponibleUsuario" type="text" class="form-control input-sm required" />
                                        </div>
									</div>
                                    <div class="row">
										<div class="col-lg-12">
										<label>Seleccion de m&aacute;quinas </label>
										<label id="cmbMaquinas-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbMaquinas[]" id="cmbMaquinas" class="form-control dual_select required" multiple style=" background-color:#0000CC">
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>
									
									<div class="row" style="margin-top:20px">
										<div class="col-lg-12">
										<label>Seleccion de ubicaciones </label>
										<label id="cmbUbicaciones-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbUbicaciones[]" id="cmbUbicaciones" class="form-control dual_select required" multiple>
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>
									
									<div class="row" style="margin-top:20px">
										<div class="col-lg-12">
										<label>Seleccion de grupos de usuarios </label>
										<label id="cmbUsuarios-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbUsuarios[]" id="cmbUsuarios" class="form-control dual_select" multiple>
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>
									
									<div class="row" style="margin-top:20px">
										<div class="col-lg-12">
										<label>Seleccion de productos </label>
										<label id="cmbProductos-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbProductos[]" id="cmbProductos" class="form-control dual_select" multiple>
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>
									
									<div class="row" style="margin-top:20px">
										<div class="col-lg-12">
										<label>Seleccion de categor&iacute;a </label>
										<label id="cmbCategorias-error" style="display:none">Este campo es requerido. Seleccione al menos un elemento.</label>
										<div class="ibox-content">
											<select name="cmbCategorias[]" id="cmbCategorias" class="form-control dual_select" multiple>
												<option value="United States">United States</option>
												<option value="United Kingdom">United Kingdom</option>
												<option value="Australia">Australia</option>
												<option value="Austria">Austria</option>
												<option value="Bahamas">Bahamas</option>
												<option value="Barbados">Barbados</option>
												<option value="Belgium">Belgium</option>
												<option value="Bermuda">Bermuda</option>
												<option value="Brazil">Brazil</option>
												<option value="Bulgaria">Bulgaria</option>
												<option value="Cameroon">Cameroon</option>
												<option value="Canada">Canada</option>
											</select>
										</div>
										</div>
									</div>
                                </fieldset>

                                <h1>Confirmaci&oacute;n</h1>
                                <fieldset>
									 <h2>Confirmaci&oacute;n</h2>
									 <DIV class="row">
									  <div class="col-lg-12">
                                     		<div class="form-group">
                                                <label>Aplicar descuento </label>
												<div class="row">
													<div class="col-lg-3">
														<div class="i-checks"><label> <input type="radio" id="chkDescuento_1" onchange="MuestraCantidad(1)" value="1" name="chkDescuento"> <i></i> <span style="font-weight:normal">Porcentaje (%)</span> </label></div>
													</div>
													<div class="col-lg-3">
													 	<div class="i-checks"><label> <input type="radio" id="chkDescuento_2" onchange="MuestraCantidad(2)" value="2" name="chkDescuento"> <i></i> <span style="font-weight:normal">Cantidad</span> </label></div>
													</div>
													<div class="col-lg-3">
													 	<div class="i-checks"><label> <input type="radio" id="chkDescuento_3" onchange="MuestraCantidad(3)" checked="" value="3" name="chkDescuento"> <i></i> <span style="font-weight:normal">Ninguno </span></label></div>
													</div>
												 </div>
                                            </div>
											<div class="form-group col-lg-3" style="display:none" id="lineaCantidad">
												
												<label>Cantidad %</label>
												<input type="text" class="form-control input-sm" />
											</div>
										</div>
											 <div class="col-lg-12">
											<div class="form-group">
                                                <label>Regalar un producto </label>
												<div class="row">
													<div class="col-lg-3">
														<div class="i-checks"><label> <input type="radio"  value="option2" name="chkRegalar"> <i></i> <span style="font-weight:normal">Si</span> </label></div>
													</div>
													<div class="col-lg-3">
													 	<div class="i-checks"><label> <input type="radio" checked="checked" value="option2" name="chkRegalar"> <i></i> <span style="font-weight:normal">No</span> </label></div>
													</div>
												 </div>
                                            </div>
										</div>
									</DIV>
                                </fieldset>

                              
                            </form>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
<script src="js/plugins/steps/jquery.steps.min.js?v=2"></script>
<!-- Jquery Validate -->
<script src="js/plugins/validate/jquery.validate.min.js?v=2"></script>

  <script>
  
  
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
					if(currentIndex == 0){
						 $('.chosen-select').chosen({width: "100%"});
						 $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#reportrange').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: { days: 60 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Hoy': [moment(), moment()],
                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '�ltimos 7 d�as': [moment().subtract(6, 'days'), moment()],
                    '�ltimos 30 d�as': [moment().subtract(29, 'days'), moment()],
                    'Este mes': [moment().startOf('month'), moment().endOf('month')],
                    'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Aplicar',
                    cancelLabel: 'Cancelar',
                    fromLabel: 'Desde',
                    toLabel: 'hasta',
                    customRangeLabel: 'Personalizar',
                    daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    firstDay: 1
                }
            }, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });
						 
						 
						 
					}
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
				
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }
                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";
					$("#cmbMaquinas-error").removeClass("error");
					$("#cmbUbicaciones-error").removeClass("error");
					$("#cmbUsuarios-error").removeClass("error");
					$("#cmbProductos-error").removeClass("error");
					$("#cmbCategorias-error").removeClass("error");
					
					$("#cmbMaquinas-error").hide();
					$("#cmbUbicaciones-error").hide();
					$("#cmbUsuarios-error").hide();
					$("#cmbProductos-error").hide();
					$("#cmbCategorias-error").hide();
					
					if(currentIndex == 1){
						
						 var indice = document.getElementById("cmbMaquinas").selectedIndex;
						 //alert(indice);
	                     if( indice == -1) {
						 	//alert("Seleccione una maquina");
							$("#cmbMaquinas-error").addClass("error");
							$("#cmbMaquinas-error").show();
							//$("#cmbMaquinas").addClass("input.error");
							//$(".body:eq(" + newIndex + ") label.error", form).remove();
							return;
						 }
						 
						  var indice = document.getElementById("cmbUbicaciones").selectedIndex;
						 //alert(indice);
	                     if( indice == -1) {
							$("#cmbUbicaciones-error").addClass("error");
							$("#cmbUbicaciones-error").show();
							//$("#cmbMaquinas").addClass("input.error");
							//$(".body:eq(" + newIndex + ") label.error", form).remove();
							return;
						 }
						 
						  var indice = document.getElementById("cmbUsuarios").selectedIndex;
	                     if( indice == -1) {
							$("#cmbUsuarios-error").addClass("error");
							$("#cmbUsuarios-error").show();
							//$("#cmbMaquinas").addClass("input.error");
							//$(".body:eq(" + newIndex + ") label.error", form).remove();
							return;
						 }
						 
						  var indice = document.getElementById("cmbProductos").selectedIndex;
						 //alert(indice);
	                     if( indice == -1) {
						 	//alert("Seleccione una maquina");
							$("#cmbProductos-error").addClass("error");
							$("#cmbProductos-error").show();
							//$("#cmbMaquinas").addClass("input.error");
							//$(".body:eq(" + newIndex + ") label.error", form).remove();
							return;
						 }
						 
						  var indice = document.getElementById("cmbCategorias").selectedIndex;
						 //alert(indice);
	                     if( indice == -1) {
						 	//alert("Seleccione una maquina");
							$("#cmbCategorias-error").addClass("error");
							$("#cmbCategorias-error").show();
							//$("#cmbMaquinas").addClass("input.error");
							//$(".body:eq(" + newIndex + ") label.error", form).remove();
							return;
						 }
					
					}
                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
					
					$("#chkDescuento_1").click(function(){ alert("you are here");});
					
       });
    </script>
    <script src="js/plugins/iCheck/icheck.min.js"></script>
	<script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
				
				$('.dual_select').bootstrapDualListbox({
                	selectorMinimalHeight: 160
            	});
				
            });
        </script>
	 <script src="js/plugins/dualListbox/jquery.bootstrap-duallistbox.js?v=21"></script>

<?php include_once "footer.inc.php"; ?>