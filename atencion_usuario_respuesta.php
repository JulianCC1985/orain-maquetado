<?php include_once "header.inc.php"; ?>

 <div class="row wrapper border-bottom page-heading">
                <div class="col-lg-10">
                    <h2><i class="fa fa-comments"></i> Atenci&oacute;n al usuario</h2>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
 <div class="wrapper wrapper-content">
 	<div class="row">
			  <div class="col-lg-9 ">
            <div class="mail-box-header">
                <div class="pull-right">
                    <a href="atencion_usuario.php" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-inbox"></i> Bandeja de entrada</a>
                </div>
                <h2>
                    Responder mensaje
                </h2>
            </div>
                <div class="mail-box">


                <div class="mail-body">

                    <form class="form-horizontal" method="get">
                        <div class="form-group"><label class="col-sm-2 control-label">Para:</label>

                            <div class="col-sm-10"><input type="text" class="form-control" value="alex.smith@corporat.com"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Asunto:</label>

                            <div class="col-sm-10"><input type="text" class="form-control" value=""></div>
                        </div>
                        </form>

                </div>

                    <div class="mail-text h-200">

                        <div class="summernote">
                            <h3>Hello Jonathan! </h3>
                            dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's</strong> standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                            typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                            <br/>
                            <br/>

                        </div>
<div class="clearfix"></div>
                        </div>
                    <div class="mail-body text-right">
                        <a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"><i class="fa fa-reply"></i> Enviar</a>
                    </div>
                    <div class="clearfix"></div>



                </div>
            </div>

           
			<div class="col-lg-3">
				<div class="widget style1 navy-bg">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span> Pendientes </span>
                            <h2 class="font-bold">100</h2>
                        </div>
                    </div>
                </div>
			
				<h3 class="font-bold">Tipolog&iacute;a de mensajes</h3>
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
				
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
				
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
				
				<div class="widget style1 lazur-bg" style="background-color:#999999">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-envelope-o fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span>Tipo </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
			
			</div>
            </div>
  


</div>

<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title">Responder mensaje</h4>
                                            <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                                        </div>
                                        <div class="modal-body">
                                            
											<div class="feed-activity-list" style="border-bottom:none">
												<div class="feed-element" style="border-bottom:none">
													<a href="#" class="pull-left">
														<img alt="image" class="img-circle" src="img/a2.jpg">
													</a>
													<div class="media-body ">
														<small class="pull-right">2h ago</small>
														<strong>Mark Johnson</strong> posted message on <strong>Monica Smith</strong> site. <br>
														<small class="text-muted">Today 2:10 pm - 12.06.2014</small>
														<div class="well">
															Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
															Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
														</div>
														<div>
															<textarea class="form-control" style="resize: none" rows="5"></textarea>
														</div>
													</div>
												</div>
											</div>
											
											
											
											
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <button type="button" class="btn btn-primary">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
<script src="js/plugins/dataTables/datatables.min.js?v=2.33"></script>
<script src="js/plugins/summernote/summernote.min.js"></script>
<script>
function MuestraRespuesta(id){
	$("#linea_"+id).show();
}

$(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ]

            });

        });
</script>
 <script src="js/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
			 $('.summernote').summernote();
        });
    </script>
<?php include_once "footer.inc.php"; ?>