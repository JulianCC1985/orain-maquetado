<?php include_once "header.inc.php"; ?>
<style>
.cover{ 
	position:fixed; 
	top:0; left:0; 
	background:rgba(0,0,0,0.6); 
	z-index:9999999999999999999; 
	width:100%; 
	height:100%;
	filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#4c000000', endColorstr='#4c000000'); 
	background-image:url(../../multimedia/img/system/bg.png);
}
.cover_up{
	
}
</style>
<div class="cover">
	<div class="cover_up">
		 <div class="col-lg-3"></div>
		 <div class="col-lg-6 text-center loginscreen   animated fadeInDown">
		  <div style="margin-top:120px">
			  <div class="ibox-content">
			  	<h2>Completa la informaci&oacute;n</h2>
				<div class="row">
					 <div class="col-lg-12"> 
						 <div class="form-group">
							   <input type="text" class="form-control input-sm" placeholder="Nombre de la empresa" required="">
						 </div>
						 <div class="form-group">
							   <input type="text" class="form-control input-sm" placeholder="NIF/CIF" required="">
						 </div>
						 <div class="form-group">
							   <input type="text" class="form-control input-sm" placeholder="Direcci&oacute;n" required="">
						 </div>
					 </div>
			  	</div>
				
				<div class="row">
					 <div class="col-lg-3"> 
					 	 <div class="form-group">
							   <input type="text" class="form-control input-sm" placeholder="Ciudad" required="">
						 </div>
					 </div>
					 <div class="col-lg-3"> 
					 	 <div class="form-group">
							   <input type="text" class="form-control input-sm" placeholder="Provincia/Estado" required="">
						 </div>
					 </div>
					 <div class="col-lg-3"> 
					 	 <div class="form-group">
							   <input type="text" class="form-control input-sm" placeholder="C&oacute;digo Postal" required="">
						 </div>
					 </div>
					 
					 <div class="col-lg-3"> 
					 	 <div class="form-group">
							   <input type="text" class="form-control input-sm" placeholder="Pa&iacute;s" required="">
						 </div>
					 </div>
				</div>
				
				<div class="row">
					 <div class="col-lg-3"> 
					 	 <div class="form-group">
							   <input type="text" class="form-control input-sm" placeholder="Tel&eacute;fono" required="">
						 </div>
					 </div>
					 
					 <div class="col-lg-3"> 
					 	 <div class="form-group">
							  <select class="form-control input-sm">
							  	<option>Vending</option>
								<option>OCS</option>
								<option>Todos</option>
							  </select>
						 </div>
					 </div>
				</div>
				<div class="row">
					
					<div class="col-lg-6"> 
					<div class="form-group" style="text-align:left">
                                          
												<div class="row">
													<div class="col-lg-6">
														<div class="i-checks"><label> <input type="radio" checked="" value="option2" name="a"> <i></i> <span style="font-weight:normal">Operador </span></label></div>
													</div>
													<div class="col-lg-6">
													 	<div class="i-checks"><label> <input type="radio" value="option2" name="a"> <i></i> <span style="font-weight:normal">Distribuidor</span> </label></div>
													</div>
												 </div>
                                            </div>
					</div>
						
				</div>
				<div class="row">
					 <div class="col-lg-3"> 
				 <button class="btn btn-primary block full-width m-b ">Guardar</button>
					</div>
				</div>
			  </div>
			</div>
		</div>
		 <div class="col-lg-3"></div>
	</div>
</div>
<div class="wrapper wrapper-content">
   <div class="tabs-container">
      <ul class="nav nav-tabs">
         <li <?php if($_GET["tipo"] == "vending" || !$_GET["tipo"]) {?>class="active" <?php }?> onclick="location.href='index.php?tipo=vending'"><a data-toggle="tab" href="#tab-1"> Vending</a></li>
         <li <?php if($_GET["tipo"] == "ocs") {?>class="active" <?php }?> onclick="location.href='index.php?tipo=ocs'"><a data-toggle="tab" href="#tab-2">OCS</a></li>
      </ul>
      <div class="tab-content">
         <div id="tab-1" class="tab-pane active">
            <div class="row" style="margin-top:15px">
               <div class="col-lg-4">
                  <div class="ibox float-e-margins">
                     <div class="ibox-title">
                        <span class="label label-success pull-right" style="background-color:#999999">Hoy</span>
                        <h5>Ingresos del d&iacute;a</h5>
                     </div>
                     <div class="ibox-content">
                        <h1 class="no-margins">40 886,200</h1>
                        <div class="stat-percent font-bold text-success" style="color:#999999">98% <i class="fa fa-arrows-h"></i></div>
                        <small>Total income</small>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="ibox float-e-margins">
                     <div class="ibox-title">
                        <span class="label label-info pull-right" style="background-color:#999999">Este mes</span>
                        <h5>Usuarios Activos</h5>
                     </div>
                     <div class="ibox-content">
                        <h1 class="no-margins">275,800</h1>
                        <div class="stat-percent font-bold text-info" style=" color:#009900">20% <i class="fa fa-level-up"></i></div>
                        <small>New orders</small>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="ibox float-e-margins">
                     <div class="ibox-title">
                        <span class="label label-primary pull-right" style="background-color:#999999">Este mes</span>
                        <h5>Ingresos/M&aacute;quina</h5>
                     </div>
                     <div class="ibox-content">
                        <h1 class="no-margins">106,120</h1>
                        <div class="stat-percent font-bold text-navy" style=" color:#FF0000">44% <i class="fa fa-level-down"></i></div>
                        <small>New visits</small>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                     <div class="ibox-title">
                        <h5>Ingresos totales - Total usuarios</h5>
                        <div class="pull-right" style="display:none">
                           <div class="btn-group">
                              <button type="button" class="btn btn-xs btn-white active">Today</button>
                              <button type="button" class="btn btn-xs btn-white">Monthly</button>
                              <button type="button" class="btn btn-xs btn-white">Annual</button>
                           </div>
                        </div>
                     </div>
                     <div class="ibox-content">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="flot-chart">
                                 <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                              </div>
                           </div>
                           <div class="col-lg-3" style="display:none">
                              <ul class="stat-list">


                                 <li>
                                    <h2 class="no-margins">2,346</h2>
                                    <small>Total orders in period</small>
                                    <div class="stat-percent">48% <i class="fa fa-level-up text-navy"></i></div>
                                    <div class="progress progress-mini">
                                       <div style="width: 48%;" class="progress-bar"></div>
                                    </div>
                                 </li>
                                 <li>
                                    <h2 class="no-margins ">4,422</h2>
                                    <small>Orders in last month</small>
                                    <div class="stat-percent">60% <i class="fa fa-level-down text-navy"></i></div>
                                    <div class="progress progress-mini">
                                       <div style="width: 60%;" class="progress-bar"></div>
                                    </div>
                                 </li>
                                 <li>
                                    <h2 class="no-margins ">9,180</h2>
                                    <small>Monthly income from orders</small>
                                    <div class="stat-percent">22% <i class="fa fa-bolt text-navy"></i></div>
                                    <div class="progress progress-mini">
                                       <div style="width: 22%;" class="progress-bar"></div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php if($_GET["tipo"] == "ocs") {?>
            <div class="row">
               <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                     <div class="ibox-title">
                        <h5>Estado de los pedidos</h5>
                        <div class="ibox-tools">
                           <a class="collapse-link">
                           <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="close-link">
                           <i class="fa fa-times"></i>
                           </a>
                        </div>
                     </div>
                     <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                              <thead>
                                 <tr>
                                    <th>C&oacute;digo</th>
                                    <th>Nombre del grupo</th>
                                    <th>Estado</th>
                                    <th>Precio</th>
                                    <th>Fecha</th>
                                    <th>Marcar enviados</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>15-877</td>
                                    <td>Orain OCS</td>
                                    <td>Enviado</td>
                                    <td>12 &euro;</td>
                                    <td>2017-05-05 08:02:35</td>
									<td></td>
                                 </tr>
                                 
                              </tbody>
							  <tfoot>
                                <tr>
									 <td colspan="2">
									<small>Mostrando 1 de 1 de 1 entrada</small>
									 </td>
                                    <td colspan="6">
										<small>
                                        <ul class="pagination pull-right"></ul>
										</small>
                                    </td>
                                </tr>
                                </tfoot>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                     <div class="ibox-title">
                        <h5>Informaci&oacute;n de grupos</h5>
                        <div class="ibox-tools">
                           <a class="collapse-link">
                           <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="close-link">
                           <i class="fa fa-times"></i>
                           </a>
                        </div>
                     </div>
                     <div class="ibox-content">
                        <div class="table-responsive">
                           <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                              <thead>
                                 <tr>
                                    <th>Identificador de m&aacute;quinas</th>
                                    <th>Saldo</th>
                                    <th style="text-align:center">Hist&oacute;rico compras</th>
                                    <th style="text-align:center">Hist&oacute;rico consumiciones</th>
                                    <th>Matr&iacute;cula</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>ORAIN-OCS</td>
									<td>0 &euro;</td>
									<td style="text-align:center"><i class="fa fa-line-chart" aria-hidden="true"></i></td>
									<td style="text-align:center"><i class="fa fa-line-chart" aria-hidden="true"></i></td>
									<td>VRM-30-30</td>
                                 </tr>
                                <tfoot>
                                <tr>
									 <td colspan="2">
									<small>Mostrando 1 de 1 de 1 entrada</small>
									 </td>
                                    <td colspan="5">
                                      <small>
                                        <ul class="pagination pull-right"></ul>
										</small>
                                    </td>
                                </tr>
                                </tfoot>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                     <div class="ibox-title">
                        <h5>Informaci&oacute;n de usuarios</h5>
                        <div class="ibox-tools">
                           <a class="collapse-link">
                           <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="close-link">
                           <i class="fa fa-times"></i>
                           </a>
                        </div>
                     </div>
                     <div class="ibox-content">
                        <div class="table-responsive">
                             <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                              <thead>
                                 <tr>
                                    <th>Nombre usuario</th>
                                    <th>Grupo</th>
                                    <th>Saldo</th>
                                    <th style="text-align:center">Consumiciones</th>
                                    <th style="text-align:center">Recarga</th>
                                    <th>Email</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Xavier</td>
                                    <td>Orain-OCS</td>
                                    <td>0 &euro;</td>
									<td style="text-align:center"><i class="fa fa-table" aria-hidden="true"></i></td>
									<td style="text-align:center"><i class="fa fa-table" aria-hidden="true"></i></td>
                          			<td>juliancabanillas@gmail.com</td>
                                 </tr>
                                
                              </tbody>
							  <tfoot>
                                <tr>
									 <td colspan="2">
									 <small>Mostrando 1 de 1 de 1 entrada</small>
									 </td>
                                    <td colspan="5">
                                        <small>
                                        <ul class="pagination pull-right"></ul>
										</small>
                                    </td>
                                </tr>
                                </tfoot>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php }?>
            <div class="row">
               <div class="col-lg-4">
                  <div class="ibox float-e-margins">
                     <div class="ibox-title">
                        <h5>Mensajes recibidos</h5>
                        <div class="ibox-tools">
                           <a class="collapse-link">
                           <i class="fa fa-chevron-up"></i>
                           </a>
                           <a class="close-link">
                           <i class="fa fa-times"></i>
                           </a>
                        </div>
                     </div>
                     <div class="ibox-content ibox-heading">
                        <h3><i class="fa fa-envelope-o"></i> Nuevos mensajes</h3>
                        <small><i class="fa fa-tim"></i> Tienes 22 nuevos mensajes.</small>
                     </div>
                     <div class="ibox-content">
                        <div class="feed-activity-list">
                           <div class="feed-element">
                              <div>
                                 <small class="pull-right text-navy">1m ago</small>
                                 <strong>Monica Smith</strong> ha escrito en <strong>m&aacute;quina</strong>
                                 <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</div>
                                 <small class="text-muted">Today 5:60 pm - 12.06.2014</small>
                              </div>
                           </div>
                           <div class="feed-element">
                              <div>
                                 <small class="pull-right">2m ago</small>
                                 <strong>Monica Smith</strong> ha escrito en <strong>m&aacute;quina</strong>
                                 <div>There are many variations of passages of Lorem Ipsum available</div>
                                 <small class="text-muted">Today 2:23 pm - 11.06.2014</small>
                              </div>
                           </div>
                           <div class="feed-element">
                              <div>
                                 <small class="pull-right">5m ago</small>
                                 <strong>Monica Smith</strong> ha escrito en <strong>m&aacute;quina</strong>
                                 <div>Contrary to popular belief, Lorem Ipsum</div>
                                 <small class="text-muted">Today 1:00 pm - 08.06.2014</small>
                              </div>
                           </div>
                           <div class="feed-element">
                              <div>
                                 <small class="pull-right">5m ago</small>
                                 <strong>Monica Smith</strong> ha escrito en <strong>m&aacute;quina</strong>
                                 <div>The generated Lorem Ipsum is therefore </div>
                                 <small class="text-muted">Yesterday 8:48 pm - 10.06.2014</small>
                              </div>
                           </div>
                           <div class="feed-element">
                              <div>
                                 <small class="pull-right">5m ago</small>
                                 <strong>Monica Smith</strong> ha escrito en <strong>m&aacute;quina</strong>
                                 <div>All the Lorem Ipsum generators on the Internet tend to repeat </div>
                                 <small class="text-muted">Yesterday 8:48 pm - 10.06.2014</small>
                              </div>
                           </div>
                           <button class="btn btn-primary btn-block m-t" onclick="location.href= 'atencion_usuario.php'"><i class="fa fa-arrow-down"></i> Ver m&aacute;s</button>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-8">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                           <div class="ibox-title">
                              <h5>Hist&oacute;rico Promociones</h5>
                              <div class="ibox-tools">
                                 <a class="collapse-link">
                                 <i class="fa fa-chevron-up"></i>
                                 </a>
                                 <a class="close-link">
                                 <i class="fa fa-times"></i>
                                 </a>
                              </div>
                           </div>
                           <div class="ibox-content">
                              <div class="table-responsive">
                                 <table class="table table-hover no-margins">
                                    <thead>
                                       <tr>
                                          <th>Nombre</th>
                                          <th>Estado</th>
                                          <th>Sector</th>
                                          <th>Fecha</th>
                                          <th>Mensajes enviados</th>
                                          <th>Porcentaje le&iacute;dos</th>
                                          <th>Porcentaje conversi&oacute;n</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td><small>Pending...</small></td>
                                          <td>11:20pm</td>
                                          <td>Samantha</td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                       </tr>
                                       <tr>
                                          <td><small>Pending...</small></td>
                                          <td>11:20pm</td>
                                          <td>Samantha</td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                       </tr>
                                       <tr>
                                          <td><small>Pending...</small></td>
                                          <td>11:20pm</td>
                                          <td>Samantha</td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                       </tr>
                                       <tr>
                                          <td><small>Pending...</small></td>
                                          <td>11:20pm</td>
                                          <td>Samantha</td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                       </tr>
                                       <tr>
                                          <td><small>Pending...</small></td>
                                          <td>11:20pm</td>
                                          <td>Samantha</td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                          <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                              <button class="btn btn-primary btn-block m-t" onclick="location.href = 'crear_campania.php'"><i class="fa fa-plus"></i> Crear nueva campa&ntilde;a</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="js/plugins/iCheck/icheck.min.js"></script>
<script src="js/plugins/footable/footable.all.min.js"></script>
<script>
   $(document).ready(function() {
   
   		 $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
   		 $('.footable').footable();
       $('.chart').easyPieChart({
           barColor: '#f8ac59',
   //                scaleColor: false,
           scaleLength: 5,
           lineWidth: 4,
           size: 80
       });
   
       $('.chart2').easyPieChart({
           barColor: '#1c84c6',
   //                scaleColor: false,
           scaleLength: 5,
           lineWidth: 4,
           size: 80
       });
   
       var data2 = [
           [gd(2012, 1, 1), 7], [gd(2012, 1, 2), 6], [gd(2012, 1, 3), 4], [gd(2012, 1, 4), 8],
           [gd(2012, 1, 5), 9], [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
           [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6],
           [gd(2012, 1, 13), 4], [gd(2012, 1, 14), 5], [gd(2012, 1, 15), 11], [gd(2012, 1, 16), 8],
           [gd(2012, 1, 17), 8], [gd(2012, 1, 18), 11], [gd(2012, 1, 19), 11], [gd(2012, 1, 20), 6],
           [gd(2012, 1, 21), 6], [gd(2012, 1, 22), 8], [gd(2012, 1, 23), 11], [gd(2012, 1, 24), 13],
           [gd(2012, 1, 25), 7], [gd(2012, 1, 26), 9], [gd(2012, 1, 27), 9], [gd(2012, 1, 28), 8],
           [gd(2012, 1, 29), 5], [gd(2012, 1, 30), 8], [gd(2012, 1, 31), 25]
       ];
   
       var data3 = [
           [gd(2012, 1, 1), 800], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
           [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
           [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700],
           [gd(2012, 1, 13), 500], [gd(2012, 1, 14), 600], [gd(2012, 1, 15), 700], [gd(2012, 1, 16), 786],
           [gd(2012, 1, 17), 345], [gd(2012, 1, 18), 888], [gd(2012, 1, 19), 888], [gd(2012, 1, 20), 888],
           [gd(2012, 1, 21), 987], [gd(2012, 1, 22), 444], [gd(2012, 1, 23), 999], [gd(2012, 1, 24), 567],
           [gd(2012, 1, 25), 786], [gd(2012, 1, 26), 666], [gd(2012, 1, 27), 888], [gd(2012, 1, 28), 900],
           [gd(2012, 1, 29), 178], [gd(2012, 1, 30), 555], [gd(2012, 1, 31), 993]
       ];
   
   
       var dataset = [
           {
               label: "Ventas del mes",
               data: data3,
               color: "#e99313",
               bars: {
                   show: true,
                   align: "center",
                   barWidth: 24 * 60 * 60 * 600,
                   lineWidth:0
               }
   
           }, {
               label: "Usuarios activos del mes",
               data: data2,
               yaxis: 2,
               color: "#666666",
               lines: {
                   lineWidth:1,
                       show: true,
                       fill: true,
                   fillColor: {
                       colors: [{
                           opacity: 0.2
                       }, {
                           opacity: 0.4
                       }]
                   }
               },
               splines: {
                   show: false,
                   tension: 0.6,
                   lineWidth: 1,
                   fill: 0.1
               },
           }
       ];
   
   
       var options = {
           xaxis: {
               mode: "time",
               tickSize: [3, "day"],
               tickLength: 0,
               axisLabel: "Date",
               axisLabelUseCanvas: true,
               axisLabelFontSizePixels: 12,
               axisLabelFontFamily: 'Arial',
               axisLabelPadding: 10,
               color: "#d5d5d5"
           },
           yaxes: [{
               position: "left",
               max: 1070,
               color: "#d5d5d5",
               axisLabelUseCanvas: true,
               axisLabelFontSizePixels: 12,
               axisLabelFontFamily: 'Arial',
               axisLabelPadding: 3
           }, {
               position: "right",
               clolor: "#d5d5d5",
               axisLabelUseCanvas: true,
               axisLabelFontSizePixels: 12,
               axisLabelFontFamily: ' Arial',
               axisLabelPadding: 67
           }
           ],
           legend: {
               noColumns: 1,
               labelBoxBorderColor: "#000000",
               position: "nw"
           },
           grid: {
               hoverable: false,
               borderWidth: 0
           }
       };
   
       function gd(year, month, day) {
           return new Date(year, month - 1, day).getTime();
       }
   
       var previousPoint = null, previousLabel = null;
   
       $.plot($("#flot-dashboard-chart"), dataset, options);
   
       var mapData = {
           "US": 298,
           "SA": 200,
           "DE": 220,
           "FR": 540,
           "CN": 120,
           "AU": 760,
           "BR": 550,
           "IN": 200,
           "GB": 120,
       };
   
       $('#world-map').vectorMap({
           map: 'world_mill_en',
           backgroundColor: "transparent",
           regionStyle: {
               initial: {
                   fill: '#e4e4e4',
                   "fill-opacity": 0.9,
                   stroke: 'none',
                   "stroke-width": 0,
                   "stroke-opacity": 0
               }
           },
   
           series: {
               regions: [{
                   values: mapData,
                   scale: ["#1ab394", "#22d6b1"],
                   normalizeFunction: 'polynomial'
               }]
           },
       });
   });
</script>
<?php include_once "footer.inc.php"; ?>