<?php include_once "header.inc.php"; ?>
<style>
.precio{
	color:#FF0000; font-weight:bold; font-size:14px;
	text-align:center
}
.linea {
	padding:10px;
	border-top:solid 1px #ccc
}
</style>
<div class="row wrapper border-bottom page-heading">
   <div class="col-lg-10">
      <h2><i class="fa fa-sticky-note-o"></i> Facturaci&oacute;n</h2>
   </div>
   <div class="col-lg-2">
   </div>
</div>
<div class="wrapper wrapper-content">

<div class="tabs-container">
    <ul class="nav nav-tabs">
         <li class="active"><a data-toggle="tab" href="#tab-1"> Tarifa</a></li>
         <li class=""><a data-toggle="tab" href="#tab-2">TPV</a></li>
		  <li class=""><a data-toggle="tab" href="#tab-3">Cuentas corrientes</a></li>
		   <li class=""><a data-toggle="tab" href="#tab-4">Mis facturas</a></li>
	</ul>
	
		<div class="tab-content">
            <div id="tab-1" class="tab-pane active">
				<div class="row">
					<div class="col-lg-12">
							 <div class="ibox">
							  <div class="ibox-content">
								 <div class="row">
								 
									
		
       								<div class="col-lg-3">	
										<div class="row">		
											<div class="col-lg-2" style="padding-top:25px">		
													<div class="i-checks"><label> <input type="checkbox" value=""> <i></i> </label></div>
											</div>	
											<div class="col-lg-10">		
												<div class="widget style1 navy-bg">
													<div class="row">
														
														<div class="col-xs-12 text-center">
															<h3 class="font-bold">Orain b&aacute;sico</h3>
														</div>
													</div>
												</div>
											</div>
										</div>
									 </div>	
									 
									 <div class="col-lg-7">	
									 <ul>
									 	<li>Sistema de pago a trav&eacute;s del m&oacute;vil</li>
										<li>Promoci&oacute;n descarga</li>
									 	<li>Datos b&aacute;sicos</li>
										<li>Centro de resoluci&oacute;n de incidencias</li>
									 </ul>
									 </div>	
									 
									 <div class="col-lg-2 precio">	
										<span style="font-size:32px">3,5&euro;</span> maq/mes<BR />+IVA
									 </div>							 
								 
			 
								 </div>
								 
								 <div class="row" style="border-bottom: solid 1px #eee; margin-bottom:10px"></div>
								  <div class="row">
								 
									 
		
       								<div class="col-lg-3">		
																
										<div class="row">		
											<div class="col-lg-2" style="padding-top:25px">		
													<div class="i-checks"><label> <input type="checkbox" value=""> <i></i> </label></div>
											</div>	
											<div class="col-lg-10">		
												<div class="widget style1 navy-bg">
													<div class="row">
														
														<div class="col-xs-12 text-center">
															<h3 class="font-bold">Orain datos</h3>
														</div>
													</div>
												</div>
											</div>
										</div>
									 </div>	
									 
									  <div class="col-lg-7">	
									  	<ul>
											 <li>Ventas por m&aacute;quinas</li>
									 		<li>Ventas por usuarios</li>
									 		<li>Ventas por productos</li>
										</ul>

 
									 </div>	
									 
									 <div class="col-lg-2 precio">	
										<span style="font-size:32px">4,5&euro;</span> maq/mes<BR />+IVA
									 </div>										 
								 
			 
								 </div>
								 
								  <div class="row" style="border-bottom: dashed 1px #eee; margin-bottom:10px"></div>
								 
								  <div class="row">
								 
									 
		
       								<div class="col-lg-3">								
										<div class="row">		
											<div class="col-lg-2" style="padding-top:25px">		
													<div class="i-checks"><label> <input type="checkbox" value=""> <i></i> </label></div>
											</div>	
											<div class="col-lg-10">		
												<div class="widget style1 navy-bg">
													<div class="row">
														
														<div class="col-xs-12 text-center">
															<h3 class="font-bold">B.I. Datos</h3>
														</div>
													</div>
												</div>
											</div>
										</div>
									 </div>	
									 
									   <div class="col-lg-7">	
									  	<ul>
											 <li>Optimizaci&oacute;n parque de m&aacute;quinas</li>
									 		<li>Informe personalizado</li>
										</ul>

 
									 </div>	
									 
									 <div class="col-lg-2 precio">	
										<span style="font-size:32px">1,5&euro;</span> maq/mes<BR />+IVA
									 </div>									 
								 
			 
								 </div>
								 
								  <div class="row" style="border-bottom: solid 1px #eee; margin-bottom:10px"></div>
								   <div class="row">
								 
									 
		
       								<div class="col-lg-3">								
										<div class="row">		
											<div class="col-lg-2" style="padding-top:25px">		
													<div class="i-checks"><label> <input type="checkbox" value=""> <i></i> </label></div>
											</div>	
											<div class="col-lg-10">		
												<div class="widget style1 navy-bg">
													<div class="row">
														
														<div class="col-xs-12 text-center">
															<h3 class="font-bold">Orain comunicaci&oacute;n</h3>
														</div>
													</div>
												</div>
											</div>
										</div>
									 </div>	
									 
									   <div class="col-lg-7">	
									  	<ul>
											 <li>Gestor de promociones</li>
									 		<li>Sistema de notificaciones (50 mensajes m&aacute;quina/mes)</li>
									 		<li>Respuestas inteligentes</li>
										</ul>

 
									 </div>	
									 
									 <div class="col-lg-2 precio">	
										<span style="font-size:32px">2,5&euro;</span> maq/mes<BR />+IVA
									 </div>									 
								 
			 
								 </div>
								 
								 <div class="row" style="border-bottom: dashed 1px #eee; margin-bottom:10px"></div>
								 <div class="row">
								 
									 
		
       								<div class="col-lg-3">								
										<div class="row">		
											<div class="col-lg-2" style="padding-top:25px">		
													<div class="i-checks"><label> <input type="checkbox" value=""> <i></i> </label></div>
											</div>	
											<div class="col-lg-10">		
												<div class="widget style1 navy-bg">
													<div class="row">
														
														<div class="col-xs-12 text-center">
															<h3 class="font-bold">B.I. Comunicaci&oacute;n</h3>
														</div>
													</div>
												</div>
											</div>
										</div>
									 </div>		
									 
									   <div class="col-lg-7">	
									  	<ul>
											 <li>Clasificaci&oacute;n de mensajes</li>
									 		<li>Recomendaci&oacute;n promociones</li>
										</ul>

 
									 </div>	
									 
									 <div class="col-lg-2 precio">	
										<span style="font-size:32px">1&euro;</span> maq/mes<BR />+IVA
									 </div>								 
								 
			 
								 </div>
								  
								  
								<div class="row" style=" border-top:solid 3px #ccc">
									<div class="col-lg-12">
										<div class="row">
											<div class="col-lg-8">
											<h2><b>Total de maquinas</b></h2><h1>57</h1>
											</div>
											<div class="col-lg-4">
												<div class="row" style="padding:5px">
													<div class="col-lg-6 col-xs-6 col-sm-6 col-md-6">
														Subtotal
													</div>
													<div class="col-lg-6 col-xs-6 col-sm-6 col-md-6" style="text-align:right">
													1,300 &euro;
													</div>	
												</div>
												
												<div class="row" style="padding:5px">
													<div class="col-lg-6 col-xs-6 col-sm-6 col-md-6">
														IVA
													</div>
													<div class="col-lg-6 col-xs-6 col-sm-6 col-md-6" style="text-align:right">
													200 &euro;
													</div>	
												</div>
												
												<div class="row" style="padding:5px">
													<div class="col-lg-6 col-xs-6 col-sm-6 col-md-6">
														<B>TOTAL</B>
													</div>
													<div class="col-lg-6 col-xs-6 col-sm-6 col-md-6" style="text-align:right">
													<B>1,500 &euro;</B>
													</div>	
												</div>
											</div>
										</div>	
									</div>
				</div>	 
								 
								 
							</div>
							
							
							
						</div>
						
						
					</div>
					
					
				</div>
				
			</div>
			 <div id="tab-2" class="tab-pane">
			 	<div class="row" >
					<div class="col-lg-12">
							 <div class="ibox">
							  <div class="ibox-content">
								 <div class="row">
								 
								 <div class="col-lg-12">
                              
									 <h2>Modificar datos bancarios</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>C&oacute;digo de comercio </label>
                                                <input id="txtCodigo" name="txtCodigo" type="text" class="form-control input-sm">
                                            </div>
                                            <div class="form-group">
                                                <label>Terminal </label>
												<input id="txtTerminal" name="txtTerminal" type="text" class="form-control input-sm">
                                               
                                            </div>
                                            <div class="form-group">
                                                <label>Clave </label>
                                                <input id="txtClave" name="txtClave" type="text" class="form-control input-sm">
                                            </div>
											
											<div class="form-group">
                                              
                                                 <button type="submit" class="btn btn-primary">Guardar</button>
                                            </div>
                                        </div>
                                    </div>

								 </div>
								 
								 </div>
							</div>
						</div>
					</div>
				</div>
			 
			</div>
			<div id="tab-3" class="tab-pane">
				<div class="row" >
					<div class="col-lg-12">
							 <div class="ibox">
							  <div class="ibox-content">
								 <div class="row">
								 
								 <div class="col-lg-12">
                              
									 <h2>Modificar datos</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Entidad bancaria </label>
                                                <input id="txtEntidadBancaria" name="txtEntidadBancaria" type="text" class="form-control input-sm">
                                            </div>
                                            <div class="form-group">
                                                <label>IBAN </label>
												<input id="txtIBAN" name="txtIBAN" type="text" class="form-control input-sm">
                                               
                                            </div>
                                          
											<div class="form-group">
                                              
                                                 <button type="submit" class="btn btn-primary">Guardar</button>
                                            </div>
                                        </div>
                                    </div>

								 </div>
								 
								 </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="tab-4" class="tab-pane">
				<div class="row">
					<div class="col-lg-12">
							 <div class="ibox">
							  <div class="ibox-content">
								 <div class="row">
								 
								 <div class="col-lg-12">
								 
								 
								 
                            <div class="m-b-lg">

                                <div class="input-group">
                                    <input type="text" placeholder="Buscar por folio" class=" form-control">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-white"> Buscar</button>
                                    </span>
                                </div>
                                <div class="m-t-md">


                                    <strong>61 Facturas encontradas</strong>



                                </div>

                            </div>

                            <div class="table-responsive">
                            <table class="table table-hover issue-tracker">
                                <tbody>
                                <tr>
                                   
                                    <td class="issue-info">
                                        <a href="#">
                                            ISSUE-23
                                        </a>

                                        <small>
                                                                                      Dom, 25 de agosto de 2017. 14:20 hrs

                                        </small>
                                    </td>
                                    <td>
                                       &euro;511,48
                                    </td>
                                    <td>
                                        <span class="label label-primary">Ver</span>
                                    </td>
                                </tr>
								
								<tr>
                                   
                                    <td class="issue-info">
                                        <a href="#">
                                            ISSUE-23
                                        </a>

                                        <small>
                                                                                       Dom, 25 de agosto de 2017. 14:20 hrs

                                        </small>
                                    </td>
                                    <td>
                                       &euro;511,48
                                    </td>
                                    <td>
                                        <span class="label label-primary">Ver</span>
                                    </td>
                                </tr>
								
								<tr>
                                   
                                    <td class="issue-info">
                                        <a href="#">
                                            ISSUE-23
                                        </a>

                                        <small>
                                                                                       Dom, 25 de agosto de 2017. 14:20 hrs

                                        </small>
                                    </td>
                                    <td>
                                       &euro;511,48
                                    </td>
                                    <td>
                                        <span class="label label-primary">Ver</span>
                                    </td>
                                </tr>
								
								<tr>
                                   
                                    <td class="issue-info">
                                        <a href="#">
                                            ISSUE-23
                                        </a>

                                        <small>
                                                                                       Dom, 25 de agosto de 2017. 14:20 hrs

                                        </small>
                                    </td>
                                    <td>
                                       &euro;511,48
                                    </td>
                                    <td>
                                        <span class="label label-primary">Ver</span>
                                    </td>
                                </tr>
								
								<tr>
                                   
                                    <td class="issue-info">
                                        <a href="#">
                                            ISSUE-23
                                        </a>

                                        <small>
                                                                                       Dom, 25 de agosto de 2017. 14:20 hrs

                                        </small>
                                    </td>
                                    <td>
                                       &euro;511,48
                                    </td>
                                    <td>
                                        <span class="label label-primary">Ver</span>
                                    </td>
                                </tr>
                                
                                </tbody>
								
                            </table>
                            </div>
	 
								 <div class="row">
								 	<div class="col-lg-2 col-xs-3 col-sm-3 col-md-2">Mostrar:
										<select class="form-control">
										<option value="">10</option>
										<option value="">20</option>
										<option value="">30</option>
									</select>
									</div>									
									<div class="col-lg-10 col-xs-9 col-sm-9 col-md-10" style="text-align:right">
										1-10 de 66
									</div>
								 </div>
								 
								 </div>
								 
								 
								 </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
<script src="js/plugins/iCheck/icheck.min.js"></script>
<script>
function MarcarPlan(id){
	if(document.getElementById("hrefPlan_"+id).innerHTML == "Quitar"){
		document.getElementById("plan_"+id).style.backgroundColor = "#ffffff"; 
		document.getElementById("hrefPlan_"+id).innerHTML = "Seleccionar";
	}
	else{
		document.getElementById("plan_"+id).style.backgroundColor = "#eeeeee"; 
		document.getElementById("hrefPlan_"+id).innerHTML = "Quitar";
	}
}

$(document).ready(function () {


                $('.i-checks').iCheck({

                    checkboxClass: 'icheckbox_square-green',

                    radioClass: 'iradio_square-green',

                });
});
</script>
<?php include_once "footer.inc.php"; ?>