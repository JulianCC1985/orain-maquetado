<?php include_once "header.inc.php"; ?>
<div class="row wrapper border-bottom page-heading">
     <div class="col-lg-10">
          <h2><i class="fa fa-comments"></i> Comunicaci&oacute;n / Gestor de promociones</h2>
     </div>
     <div class="col-lg-2">
	</div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
	<div class="col-lg-6" onClick="location.href = 'crear_promocion.php'" style="cursor:pointer">
                <div class="widget style1 lazur-bg" style="background-color:#666666">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-flash fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span><b> Crear</b> </span>
                            <h2 class="font-bold">Promoci&oacute;n</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6" onClick="location.href = 'crear_campania.php'" style="cursor:pointer">
                <div class="widget style1 yellow-bg" style="background-color:#666666">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-volume-up fa-5x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <span><b> Crear </b></span>
                            <h2 class="font-bold">Campa&ntilde;a</h2>
                        </div>
                    </div>
                </div>
            </div>
			
			  <div class="col-lg-12">
                <div class="widget style1 navy-bg">
                    <div class="row">
                        <div class="col-xs-4">
                           <i class="fa fa-envelope-o fa-5x"> </i><span style="font-size:30px"><b> 1,000 </b></span>
						  
                        </div>
                        <div class="col-xs-8 text-right">
							<div style="cursor:pointer">
							<span>&nbsp;</span>
                             <h2 class="font-bold"> A&ntilde;adir Saldo</h2>
							 </div>
                        </div>
                    </div>
                </div>
            </div>
			
			
	</div>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Hist&oacute;rico Promociones</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
									
									<div class="ibox-content">
										
									
									
									   <div class="table-responsive">
									   
                                        <table class="table table-hover no-margins">
                                            <thead>
											<tr>
												<td colspan="2">Mostrar
											<select class="form-control">
												<option value="10">10</option>
												<option value="20">20</option>
											</select></td>
											</tr>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Estado</th>
                                                <th>Sector</th>
                                                <th>Fecha</th>
												<th>Mensajes enviados</th>
                                                <th>Porcentaje le&iacute;dos</th>
                                                <th>Porcentaje conversi&oacute;n</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><small>Pending...</small></td>
                                                <td><i class="fa fa-clock-o"></i> 11:20pm</td>
                                                <td>Samantha</td>
                                                <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
												<td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
												<td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
												<td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                            </tr>
                                           
                                            </tbody>
                                        </table>
										</div>
										<button class="btn btn-primary btn-block m-t" onclick="location.href = 'crear_campania.php'"><i class="fa fa-plus"></i> Crear nueva campa&ntilde;a</button>
                                    </div>
								</div>
		
		
		</div>
		<div class="col-lg-6">
			<div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Hist&oacute;rico Promociones</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>
									<div class="ibox-content">
									   <div class="table-responsive">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Tipo</th>
                                                <th>Condiciones</th>
                                                <th>No. de destinatarios</th>
												
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><small>Pending...</small></td>
                                                <td><i class="fa fa-clock-o"></i> 11:20pm</td>
                                                <td>Samantha</td>
                                                <td class="text-navy"> <i class="fa fa-level-up"></i> 24% </td>
                                            </tr>
                                           
                                            </tbody>
                                        </table>
										</div>

                                    </div>
								</div>
        </div>
</div>  
	
</div>

  
					
					

<?php include_once "footer.inc.php"; ?>