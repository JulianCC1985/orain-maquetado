<?php include_once "header.inc.php"; ?>
<div class="row wrapper border-bottom page-heading">
   <div class="col-lg-10">
      <h2><i class="fa fa-database"></i> Datos / Mi negocio</h2>
   </div>
   <div class="col-lg-2">
   </div>
</div>
<div class="wrapper wrapper-content">

<div class="tabs-container">
    <ul class="nav nav-tabs">
         <li class="active"><a data-toggle="tab" href="#tab-1"> Vending</a></li>
         <li class=""><a data-toggle="tab" href="#tab-2">OCS</a></li>
	</ul>
	
	
	<div class="tab-content">
            <div id="tab-1" class="tab-pane active">
	
				<div class="row">
					<div class="col-lg-12">
					   <div class="ibox">
						  <div class="ibox-content">
							 <div class="row">
								<div class="col-md-3">
								   <div class="form-group">
									  <label class="font-normal">Maquinas</label>
									  <div>
										 <select data-placeholder="Choose a Country..." class="chosen-select input-sm"  tabindex="2">
											<option value="">Select</option>
											<option value="United States">United States</option>
											<option value="United Kingdom">United Kingdom</option>
											<option value="Afghanistan">Afghanistan</option>
											<option value="Aland Islands">Aland Islands</option>
										 </select>
									  </div>
								   </div>
								</div>
								<div class="col-md-3">
								   <div class="form-group">
									  <label class="font-normal">Ubicaciones</label>
									  <div>
										 <select data-placeholder="Choose a Country..." class="chosen-select input-sm"  tabindex="2">
											<option value="">Select</option>
											<option value="United States">United States</option>
											<option value="United Kingdom">United Kingdom</option>
											<option value="Afghanistan">Afghanistan</option>
											<option value="Aland Islands">Aland Islands</option>
										 </select>
									  </div>
								   </div>
								</div>
								<div class="col-md-12">
								   <div class="form-group">
									  <label class="font-normal">Fecha</label>
									  <div>
										 <div id="reportrange" class="form-control">
											<i class="fa fa-calendar"></i>
											<span></span> <b class="caret"></b>
										 </div>
									  </div>
								   </div>
								</div>
							 </div>
							 <div class="row">
								<div class="col-md-3">
								   <div class="form-group">
									  <div>
										 <select data-placeholder="Choose a Country..." class="form-control input-sm"  tabindex="2">
											<option value="">Ingresos</option>
										 </select>
									  </div>
								   </div>
								</div>
								<div class="col-md-9">
								   <div class="form-group" style="text-align:right">
									   <div align="right">
										 <button class="btn btn-white btn-sm" type="submit">D&iacute;a</button>
										 <button class="btn btn-white btn-sm" type="submit">Semana</button>
										 <button class="btn btn-white btn-sm" type="submit">Mes</button>
									  </div>
								   </div>
								</div>
							 </div>
							 <div class="row">
								<div class="col-lg-12">
								   <canvas id="lineChart" height="140"></canvas>
								</div>
							 </div>
						  </div>
					   </div>
					   <div class="row">
						  <div class="col-lg-12">
							 <div class="ibox float-e-margins">
								<div class="ibox-title">
								   <h5>Basic Data Tables example with responsive plugin</h5>
								   <div class="ibox-tools">
									  <a class="collapse-link">
									  <i class="fa fa-chevron-up"></i>
									  </a>
								   </div>
								</div>
								<div class="ibox-content">
								   <div class="table-responsive">
									  <table class="table table-striped table-bordered table-hover dataTables-example" >
										 <thead>
											<tr>
											   <th>M&aacute;quina</th>
											   <th>Ubicaci&oacute;n <i class="fa fa-question-circle"></i></th>
											   <th>Unidades vendidas <i class="fa fa-question-circle"></i></th>
											   <th>Ingresos <i class="fa fa-question-circle"></i></th>
											   <th>Usuarios activos/totales <i class="fa fa-question-circle"></i></th>
											   <th>Ingresos por usuario <i class="fa fa-question-circle"></i></th>
											   <th>Porcentaje de compras Orain VS Monedas VS Billetes <i class="fa fa-question-circle"></i></th>
											   <th>N&uacute;mero de incidencias <i class="fa fa-question-circle"></i></th>
											</tr>
										 </thead>
										 <tbody>
											<tr class="gradeX">
											   <td>Trident</td>
											   <td>Internet
												  Explorer 4.0
											   </td>
											   <td>Win 95+</td>
											   <td class="center">4</td>
											   <td class="center">X</td>
											   <td class="center">A</td>
											   <td class="center">A</td>
											   <td class="center">A</td>
											</tr>
											<tr class="gradeC">
											   <td>Trident</td>
											   <td>Internet
												  Explorer 5.0
											   </td>
											   <td>Win 95+</td>
											   <td class="center">5</td>
											   <td class="center">C</td>
											   <td class="center">A</td>
											   <td class="center">A</td>
											   <td class="center">A</td>
											</tr>
											<tr class="gradeA">
											   <td>Trident</td>
											   <td>Internet
												  Explorer 5.5
											   </td>
											   <td>Win 95+</td>
											   <td class="center">5.5</td>
											   <td class="center">A</td>
											   <td class="center">A</td>
											   <td class="center">A</td>
											   <td class="center">A</td>
											</tr>
										 </tbody>
									  </table>
								   </div>
								</div>
							 </div>
						  </div>
					   </div>
				</div>
			</div>
	</div><!-- TABS-->
</div>
<script>
   $('.chosen-select').chosen({width: "100%"});
   
   
   
   $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
   
   
   
              $('#reportrange').daterangepicker({
   
                  format: 'MM/DD/YYYY',
   
                  startDate: moment().subtract(29, 'days'),
   
                  endDate: moment(),
   
                  minDate: '01/01/2012',
   
                  maxDate: '12/31/2015',
   
                  dateLimit: { days: 60 },
   
                  showDropdowns: true,
   
                  showWeekNumbers: true,
   
                  timePicker: false,
   
                  timePickerIncrement: 1,
   
                  timePicker12Hour: true,
   
                  ranges: {
   
                      'Hoy': [moment(), moment()],
   
                      'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
   
                      '�ltimos 7 d�as': [moment().subtract(6, 'days'), moment()],
   
                      '�ltimos 30 d�as': [moment().subtract(29, 'days'), moment()],
   
                      'Este mes': [moment().startOf('month'), moment().endOf('month')],
   
                      'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
   
                  },
   
                  opens: 'right',
   
                  drops: 'down',
   
                  buttonClasses: ['btn', 'btn-sm'],
   
                  applyClass: 'btn-primary',
   
                  cancelClass: 'btn-default',
   
                  separator: ' to ',
   
                  locale: {
   
                      applyLabel: 'Consultar',
   
                      cancelLabel: 'Cancelar',
   
                      fromLabel: 'Desde',
   
                      toLabel: 'hasta',
   
                      customRangeLabel: 'Personalizar',
   
                      daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
   
                      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
   
                      firstDay: 1
   
                  }
   
              }, function(start, end, label) {
   
                  console.log(start.toISOString(), end.toISOString(), label);
   
                  $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
   
              });
   
   		
   
</script>
<script src="js/plugins/chartJs/Chart.min.js"></script>
<script src="js/demo/chartjs-demo.js?v=2.1"></script>
<script src="js/plugins/dataTables/datatables.min.js?v=2.33"></script>
<script>
   $(document).ready(function(){
   
               $('.dataTables-example').DataTable({
   
                   pageLength: 25,
   
                   responsive: true,
   
                   dom: '<"html5buttons"B>lTfgitp',
   
                   buttons: [
   
                       { extend: 'copy'},
   
                       {extend: 'csv'},
   
                       {extend: 'excel', title: 'ExampleFile'},
   
                       {extend: 'pdf', title: 'ExampleFile'},
   
   
   
                       {extend: 'print',
   
                        customize: function (win){
   
                               $(win.document.body).addClass('white-bg');
   
                               $(win.document.body).css('font-size', '10px');
   
   
   
                               $(win.document.body).find('table')
   
                                       .addClass('compact')
   
                                       .css('font-size', 'inherit');
   
                       }
   
                       }
   
                   ]
   
   
   
               });
   
   
   
           });
   
</script>
<?php include_once "footer.inc.php"; ?>