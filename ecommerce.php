<?php include_once "header.inc.php"; ?>

<div class="row wrapper border-bottom page-heading">

   <div class="col-lg-10">

      <h2><i class="fa fa-shopping-cart"></i> E-commerce</h2>

   </div>

   <div class="col-lg-2">

   </div>

</div>

<div class="wrapper wrapper-content">

<div class="row">

<div class="col-md-12">

   <div class="ibox-content">

      <div class="tabs-container">

         <ul class="nav nav-tabs">

            <li class="active"><a data-toggle="tab" href="#tab-1"> Pedidos</a></li>

            <li class=""><a data-toggle="tab" href="#tab-2">Productos</a></li>

         </ul>

         <div class="tab-content">

            <div id="tab-1" class="tab-pane active">
			
			 <div class=" m-b-sm border-bottom" style=" padding-top:10px">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="order_id">C&oacute;digo</label>
                            <input type="text" id="txtCodigo" name="txtCodigo" value="" placeholder="C&oacute;digo" class="input-sm form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="status">Nombre del grupo</label>
                            <input type="text" id="txtNombreGrupo" name="txtNombreGrupo" value="" placeholder="Nombre del grupo" class="input-sm form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="customer">Estado</label>
                            <input type="text" id="txtEstado" name="txtEstado" value="" placeholder="Estado" class="input-sm form-control">
                        </div>
                    </div>
					
					 <div class="col-sm-3">
					 <div class="form-group" id="data_5">
                               <label class="control-label" for="customer">Fecha</label>
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-sm form-control" name="start" value="05/14/2014"/>
                                    <span class="input-group-addon">a</span>
                                    <input type="text" class="input-sm form-control" name="end" value="05/22/2014" />
                                </div>
                            </div>
						</div>
                </div>

            </div>
			
			
			<div class="row" style="margin-top:20px">

			   	

					 

					  <div class="col-lg-12">

						 <div class="ibox float-e-margins">

							<div class="table-responsive">

							 <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">


								  <thead>

									 <tr>

										<th>C&oacute;digo</th>

										<th>Nombre de grupo</th>
										<th>Estado</th>
										<th style="text-align:center">Total precio</th>
										<th style="text-align:center">Fecha</th>

										<th style="text-align:center">Opciones</th>

									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=10; $i++){ ?>

									 <tr class="gradeC">

										<td>1-4545</td>

										<td>Ejemplo </td>
										<td>Ejemplo estado </td>
										<td style="text-align:right">10.00 </td>
										<td style="text-align:center">25/08/2017</td>

										<td class="center" style="text-align:center">
										<span class="label label-primary" style="cursor:pointer" data-toggle="modal" data-target="#myModal5" onClick="MuestraDetalle('1-4545')">Ver/Editar</span>
										<span class="label label-danger" style="cursor:pointer">Eliminar</span>
										</td>

									 </tr>

									 <?php }?>

								  </tbody>
<tr>
                                    <td colspan="7">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
							   </table>

							</div>

							

						 </div>

                  	</div>

				</div>



				

			

			

			</div>

			<div id="tab-2" class="tab-pane">

			

				<div class="row">

			   		

					 

					  <div class="col-lg-12" >

						 <div class="ibox float-e-margins">

							<div class="table-responsive" style="padding:5px; padding-top:20px">

							 <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">


								  <thead>

									 <tr>

										<th>No.</th>

										<th>Nombre del producto</th>
										<th>Descripci&oacute;n del producto</th>
										<th>M&iacute;mimo del producto</th>
										<th>M&aacute;ximo del producto</th>
										<th>Incremento del producto</th>
										<th>Consumiciones del producto</th>
										<th>Tipo</th>
										<th>Categor&iacute;as</th>

									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=10; $i++){ ?>

									 <tr class="gradeC">

										<td>1</td>

										<td>Ejemplo del producto </td>
										<td>Descripci&oacute;n </td>
										<td>5</td>
										<td>10 </td>
										<td>10</td>
										<td>1000 </td>
										<td>Ejemplo de tipo </td>
										<td>Ejemplo </td>

										

									 </tr>

									 <?php }?>

								  </tbody>
									<tr>
                                    <td colspan="10">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
							   </table>

							</div>

							

						 </div>

                  	</div>

				</div>

			

			

			</div>

		 </div>

	 </div>

	</div>

</div>



<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">

   <div class="modal-dialog modal-lg">

      <div class="modal-content">

         

         <div class="modal-body" style="background-color:#FFFFFF">

       	 <form class="m-t" role="form" action="mi_perfil.php" required="" method="post">

             <div class="row">

			   		

					 

					  <div class="col-lg-12">

						 <div class="ibox float-e-margins">
							<h1 id="tituloModal"></h1>
							<div class="table-responsive">

							 <table class=" table table-stripped toggle-arrow-tiny" data-page-size="15">

								  <thead>

									 <tr>

										<th>Producto</th>
										<th style="text-align:center">Cantidad</th>
										<th style="text-align:center">Precio</th>
										<th style="text-align:center">Total</th>


									 </tr>

								  </thead>

								  <tbody>

									 <?php for($i=1; $i<=5; $i++){ ?>

									 <tr class="gradeC">

										<td>Producto 1 </td>

										<td style="text-align:right">100 </td>
										<td style="text-align:right">100 </td>
										<td style="text-align:right">100 </td>

										

									 </tr>

									 <?php }?>
									 <tr>
									 	<td colspan="3" style="text-align:right"></td>
										<td style="text-align:right"><b style="font-size:24px">300.00</b></td>
									 </tr>

								  </tbody>
									
							   </table>

							</div>
							  <div>
								<div class="form-group">
									<label class="control-label" for="customer">Estado</label>
									<select id="cmbEstado" name="cmbEstado" class="input-sm form-control">
										<option value="1">Estado 1</option>
									</select>
								</div>
							</div>
							

						 </div>

                  	</div>

				</div>


				

			</div>

			

			

		

			 <div class="modal-footer">

            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

            <button type="submit" class="btn btn-primary">Guardar</button>

         </div>

		</form>

         </div>

      </div>

   </div>

</div>
<!-- FooTable -->
    <script src="js/plugins/footable/footable.all.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {

            $('.footable').footable();

           
			 $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });
        });
		
		function MuestraDetalle(codigo){
			$("#tituloModal").html(codigo);
		}

    </script>

<?php include_once "footer.inc.php"; ?>