<!DOCTYPE html>

<html>



<head>



    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>Nombre de la Empresa</title>



    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

	<link href="css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

	<link href="css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

	<link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">

	<link href="css/plugins/iCheck/custom.css?v=2.5" rel="stylesheet">

    <link href="css/plugins/steps/jquery.steps.css?v=2" rel="stylesheet">

    <link href="css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">

	<link href="css/plugins/dropzone/basic.css" rel="stylesheet">

    <link href="css/plugins/dropzone/dropzone.css" rel="stylesheet">

    <link href="css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="css/plugins/codemirror/codemirror.css" rel="stylesheet">

	 <link href="css/plugins/switchery/switchery.css" rel="stylesheet">

	<link href="css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
	  <link href="css/plugins/footable/footable.core.css" rel="stylesheet">
	<link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">

    <link href="css/style.css?v=34.91" rel="stylesheet">

	    <!-- Mainly scripts -->

    <script src="js/jquery-3.1.1.min.js"></script>

 

    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>



    <!-- Flot -->

    <script src="js/plugins/flot/jquery.flot.js"></script>

    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>

    <script src="js/plugins/flot/jquery.flot.spline.js"></script>

    <script src="js/plugins/flot/jquery.flot.resize.js"></script>

    <script src="js/plugins/flot/jquery.flot.pie.js"></script>

    <script src="js/plugins/flot/jquery.flot.symbol.js"></script>

    <script src="js/plugins/flot/jquery.flot.time.js"></script>



    <!-- Peity -->

    <script src="js/plugins/peity/jquery.peity.min.js"></script>

    <script src="js/demo/peity-demo.js"></script>



    <!-- Custom and plugin javascript -->

    <script src="js/inspinia.js"></script>

    <script src="js/plugins/pace/pace.min.js"></script>



    <!-- jQuery UI -->

    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>



    <!-- Jvectormap -->

    <script src="js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>

    <script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>



    <!-- EayPIE -->

    <script src="js/plugins/easypiechart/jquery.easypiechart.js"></script>



    <!-- Sparkline -->

    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->

    <script src="js/demo/sparkline-demo.js"></script>

	<script src="js/plugins/chosen/chosen.jquery.js"></script>

	 <script src="js/plugins/fullcalendar/moment.min.js?v=2"></script>
	  <script src="js/plugins/datapicker/bootstrap-datepicker.js?v=2.0"></script>

	  <script src="js/plugins/daterangepicker/daterangepicker.js"></script>

	  <script src="js/plugins/jasny/jasny-bootstrap.min.js"></script>

</head>



<body>

    <div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">

        <?php include_once "menu.php"; ?>

    </nav>



        <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom">

        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">

        <div class="navbar-header">

            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

            

        </div>

            <ul class="nav navbar-top-links navbar-right">

                <li>

                    <span class="m-r-sm text-muted welcome-message">Bienvenido a ORAIN</span>

                </li>

                <li class="dropdown">

                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">

                        <i class="fa fa-envelope"></i>  <span class="label label-warning" style="background-color:#FF0000">16</span>

                    </a>

                    <ul class="dropdown-menu dropdown-messages">

                        <li>

                            <div class="dropdown-messages-box">

                                <a href="profile.html" class="pull-left">

                                    <img alt="image" class="img-circle" src="img/a7.jpg">

                                </a>

                                <div>

                                    <small class="pull-right">46h ago</small>

                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>

                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>

                                </div>

                            </div>

                        </li>

                        <li class="divider"></li>

                        <li>

                            <div class="dropdown-messages-box">

                                <a href="profile.html" class="pull-left">

                                    <img alt="image" class="img-circle" src="img/a4.jpg">

                                </a>

                                <div>

                                    <small class="pull-right text-navy">5h ago</small>

                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>

                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>

                                </div>

                            </div>

                        </li>

                        <li class="divider"></li>

                        <li>

                            <div class="dropdown-messages-box">

                                <a href="profile.html" class="pull-left">

                                    <img alt="image" class="img-circle" src="img/profile.jpg">

                                </a>

                                <div>

                                    <small class="pull-right">23h ago</small>

                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>

                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>

                                </div>

                            </div>

                        </li>

                        <li class="divider"></li>

                        <li>

                            <div class="text-center link-block">

                                <a href="atencion_usuario.php">

                                    <i class="fa fa-envelope"></i> <strong>Ver todos los mensajes</strong>

                                </a>

                            </div>

                        </li>

                    </ul>

                </li>

                <li class="dropdown">

                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">

                        <i class="fa fa-bell"></i>  <span class="label label-primary" style="background-color:#FF0000">8</span>

                    </a>

                    <ul class="dropdown-menu dropdown-alerts">

                        <li>

                            <a href="mailbox.html">

                                <div>

                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages

                                    <span class="pull-right text-muted small">4 minutes ago</span>

                                </div>

                            </a>

                        </li>

                        <li class="divider"></li>

                        <li>

                            <a href="profile.html">

                                <div>

                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers

                                    <span class="pull-right text-muted small">12 minutes ago</span>

                                </div>

                            </a>

                        </li>

                        <li class="divider"></li>

                        <li>

                            <a href="grid_options.html">

                                <div>

                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted

                                    <span class="pull-right text-muted small">4 minutes ago</span>

                                </div>

                            </a>

                        </li>

                        <li class="divider"></li>

                        <li>

                            <div class="text-center link-block">

                                <a href="notifications.html">

                                    <strong>See All Alerts</strong>

                                    <i class="fa fa-angle-right"></i>

                                </a>

                            </div>

                        </li>

                    </ul>

                </li>





                <li>

                    <a href="login.php">

                        <i class="fa fa-sign-out"></i>&nbsp;

                    </a>

                </li>

                <li style="display:none">

                    <a class="right-sidebar-toggle">

                        <i class="fa fa-tasks"></i>

                    </a>

                </li>

            </ul>



        </nav>

        </div>