<?php
include_once "header.inc.php";
?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"></script>

<div class="row wrapper border-bottom page-heading">

   <div class="col-lg-10">

      <h2><i class="fa fa-cog"></i> Configuraci&oacute;n / M&aacute;quinas / Crear M&aacute;quinas en OCS</h2>

   </div>

   <div class="col-lg-2">

   </div>

</div>


<div class="wrapper wrapper-content">

<div class="row">

<div class="col-md-12">

   <div class="ibox-content">
   			<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>N&uacute;mero </label>
			
								  <input id="txtNumero" name="txtNumero" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Nombre </label>
			
								  <input id="ttxNombre" name="txtNombre" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Etiqueta de ubicaci&oacute;n </label>
			
								  <input id="txtUbicacion" name="txtUbicacion" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>C&oacute;digo de activaci&oacute;n </label>
			
								  <input id="txtCodigoActivacion" name="txtCodigoActivacion" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Email del administrador </label>
			
								  <input id="txtEmailAdmin" name="txtEmailAdmin" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Nombre del grupo </label>
			
								  <input id="txtNombreGrupo" name="txtNombreGrupo" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Consumiciones kit de bienvenida</label>
			
								  <input id="txtConsumiciones" name="txtConsumiciones" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Precio del kit de bienvenida (&euro;) </label>
			
								  <input id="txtPrecioKIT" name="txtPrecioKIT" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Coste nomada (&euro;) </label>
			
								  <input id="txtCosteNomada" name="txtCosteNomada" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						
						
						
						
						<div class="row">
							<div class="col-md-12">
							 <label>Ubicaci&oacute;n detallada </label>
                            <div class="google-map" id="map3"></div>
							</div>
                   		</div>
						
						<div class="row" style="margin-top:10px">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Imagen </label>
			
								<form action="#" class="dropzone" id="dropzoneForm">
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>
			
								 </div>   
			
							</div>
						</div>
					
					</div>

					<div class="col-md-6">
					
					
						<div class="row">
							<div class="col-lg-12">
							<label>Reglas extras</label>
							</div>
						</div>					
						
						
						<div class="row">
							<div class="col-lg-12">
								 <div class="form-group">
								 	<input type="checkbox" class="i-checks"> &nbsp;Gasto de env&iacute;o
								 </div>
							</div>
							<div class="col-lg-12">
								 <div class="form-group">
								 	<input type="checkbox" class="i-checks"> &nbsp;Suscripci&oacute;n
								 </div>
							</div>
							
							<div class="col-lg-12">
								 <div class="form-group">
								 	<input type="checkbox" class="i-checks"> &nbsp;Aviso de reposici&oacute;n
								 </div>
							</div>
							
						</div>
					
					
					</div>
			</div>
			
			 <div class="row">

                              <div class="form-group col-lg-2" style="margin-top:20px" >

								
                                 <button type="submit" class="btn btn-primary block full-width m-b btn-lg"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
								 
                              </div>

                     </div>
   </div>
  </div>
 </div>
</div>
 <script src="js/plugins/iCheck/icheck.min.js"></script>
<script src="js/plugins/dropzone/dropzone.js"></script>
<script>
Dropzone.options.dropzoneForm = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // MB
            dictDefaultMessage: "<small>Arrastra los archivos aqu&iacute; o da click.</small>"
        };

  var mapOptions3 = {
                center: new google.maps.LatLng(36.964645, -122.01523),
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.SATELLITE,
                // Style for Google Maps
                styles: [{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#fffffa"}]},{"featureType":"water","stylers":[{"lightness":50}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"lightness":40}]}]
            };
 var mapElement3 = document.getElementById('map3');
 var map3 = new google.maps.Map(mapElement3, mapOptions3);
 
  $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
 
</script>

<?php
include_once "footer.inc.php";
?>