<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Orain</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css?v=2" rel="stylesheet">
	 <script>
     window.onLoadCallback = function () {
        startApp();
     }
     </script>
     <script type="text/javascript" src="https://apis.google.com/js/platform.js?onload=onLoadCallback" async defer></script>
	 <script type="text/javascript" src="js/google.js?v=2"></script>
	 <style>
	 body{
	 	background-image:url(img/Fondo_login.jpg);
		-webkit-background-size: cover; 
		-moz-background-size: cover; 
		-o-background-size: cover; 
		background-size: cover;
	 }
	 </style>
</head>

<body class="gray-bg">
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '1664993556858878',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.10'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
    <div class="loginColumns animated fadeInDown" >
        <div class="row" style="margin-top:80px">

            <div class="col-md-6" style="color:#FFFFFF;">
                <h2 class="font-bold">Bienvenido a Orain</h2>

                <p>
                    Orain te mantiene informado del estado de tus máquinas en un completo y funcional dashboard en tiempo real.
                </p>

                

            </div>
            <div class="col-md-6">
                <div class="ibox-content">
				
                    <form class="m-t" role="form" action="index.php" required="">
						 <div class="tabs-container">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#tab-1"> Usuario</a></li>
								<li class=""><a data-toggle="tab" href="#tab-2">Empresa</a></li>
							</ul>
							<div class="tab-content">
                          	  <div id="tab-1" class="tab-pane active">
                              	  <div class="panel-body">
										<div class="form-group">
											<input type="email" class="form-control" placeholder="Email" required="">
										</div>
										<div class="form-group">
											<input type="password" class="form-control" placeholder="Contrase&ntilde;a" required="">
										</div>
										<button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>
										
										 <button class="btn btn-block btn-info m-t-15" type="button" style="background-color:#39579b; border:solid 1px #39579b" onClick="FB.login(function (response) { checkLoginState(); }, {scope: 'public_profile,email'});">
									<span class="pull-left">
										<i class="fa fa-facebook"></i>
									</span>
									<span class="bold">Registrate con Facebook</span>
								</button>
								
								<button class="btn btn-danger block full-width m-b" type="button" id="googlesignin" style="margin-top:13px" >
											<span class="pull-left">
												<i class="fa fa-google-plus"></i>
											</span>
											<span class="bold">Registrate con Google+</span>
										</button>
										
										
						<div align="center">
										<a href="forgot_password.php">
											<small>Olvidaste tu contrase&ntilde;a?</small>
										</a>
				</div>
										<p class="text-muted text-center">
											<small>No tienes una cuenta?</small>
										</p>
										<a class="btn btn-sm btn-white btn-block" href="register.php">Crea una cuenta</a>
								</div>
							</div>
							<div id="tab-2" class="tab-pane">
                              	 <div class="panel-body">
										<div class="form-group">
											<input type="email" class="form-control" placeholder="Email" required="">
										</div>
										<div class="form-group">
											<input type="password" class="form-control" placeholder="Contrase&ntilde;a" required="">
										</div>
										<button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>
										
										
										
										
						<div align="center">
										<a href="forgot_password.php">
											<small>Olvidaste tu contrase&ntilde;a?</small>
										</a>
				</div>
										<p class="text-muted text-center">
											<small>No tienes una cuenta?</small>
										</p>
										<a class="btn btn-sm btn-white btn-block" href="register.php">Crea una cuenta</a>
								</div>
							</div>
						</div>
                    </form>
                    
                </div>
            </div>
        </div>
        <hr/><br>
        <div class="row" style="display:none">
            <div class="col-md-6">
                &copy; Orain
            </div>
            <div class="col-md-6 text-right">
               <small>2017</small>
            </div>
        </div>
    </div>

	 <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
</body>

</html>
