<?php
include_once "header.inc.php";
?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"></script>

<div class="row wrapper border-bottom page-heading">

   <div class="col-lg-10">

      <h2><i class="fa fa-cog"></i> Configuraci&oacute;n / M&aacute;quinas / Crear M&aacute;quinas en Vending</h2>

   </div>

   <div class="col-lg-2">

   </div>

</div>


<div class="wrapper wrapper-content">

<div class="row">

<div class="col-md-12">

   <div class="ibox-content">
   			<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>N&uacute;mero </label>
			
								  <input id="txtNumero" name="txtNumero" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Nombre </label>
			
								  <input id="ttxNombre" name="txtNombre" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Etiqueta de ubicaci&oacute;n </label>
			
								  <input id="txtUbicacion" name="txtUbicacion" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Tipo </label>
			
								  <select class="form-control input-sm" required="">
								  	<option value="0">Seleccione...</option>
								  </select>
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>C&oacute;digo de activaci&oacute;n </label>
			
								  <input id="txtCodigoActivacion" name="txtCodigoActivacion" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						
						<div class="row">
							<div class="col-md-12">
							 <label>Ubicaci&oacute;n detallada </label>
                            <div class="google-map" id="map3"></div>
							</div>
                   		</div>
						
						<div class="row" style="margin-top:10px">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Imagen </label>
			
								<form action="#" class="dropzone" id="dropzoneForm">
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>
			
								 </div>   
			
							</div>
						</div>
					
					</div>

					<div class="col-md-6">
					
					
					
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Moneda </label>
			
								  <input id="txtMoneda" name="txtMoneda" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>No. decimales </label>
			
								  <select class="form-control input-sm" required="" name="cmbDecimales" id="cmbDecimales">
								  	<option value="1">0(0000.)</option>
									<option value="2">1(000.0)</option>
									<option value="3">2(00.00)</option>
									<option value="4">3(0.000)</option>
									
								  </select>
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Factor de moneda (1-255) </label>
			
								  <input id="txtFactorMoneda" name="txtFactorMoneda" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
	
								 <div class="form-group">
			
								  <label>Timeout (segundos) </label>
			
								  <input id="txtTimeout" name="txtTimeout" type="text" class="form-control input-sm" required="">
			
								 </div>   
			
							</div>
						</div>
						
						
						<div class="row">
							<div class="col-lg-12">
								 <div class="form-group">
								 	<input type="checkbox" class="i-checks"> &nbsp;Multiventa
								 </div>
							</div>
							<div class="col-lg-12">
								 <div class="form-group">
								 	<input type="checkbox" class="i-checks"> &nbsp;Devolver dinero en venta fallida
								 </div>
							</div>
							
							<div class="col-lg-12">
								 <div class="form-group">
								 	<input type="checkbox" class="i-checks"> &nbsp;Price holding
								 </div>
							</div>
							
							<div class="col-lg-12">
								 <div class="form-group">
								 	<input type="checkbox" class="i-checks"> &nbsp;Coges MBD
								 </div>
							</div>
						</div>
					
					
					</div>
			</div>
			
			<div class="row">
				<div class="col-lg-12">
					 <div class="form-group">
			
								  <label>Carriles (n&uacute;mero de carriles + capacidad) </label>
								  <div style="height:80px; border:solid 1px #eee">
								  </div>
					</div>
				</div>
			</div>
			 <div class="row">

                              <div class="form-group col-lg-2" style="margin-top:20px" >

								
                                 <button type="submit" class="btn btn-primary block full-width m-b btn-lg"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
								 
                              </div>

                     </div>
   </div>
  </div>
 </div>
</div>
 <script src="js/plugins/iCheck/icheck.min.js"></script>
<script src="js/plugins/dropzone/dropzone.js"></script>
<script>
Dropzone.options.dropzoneForm = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 10, // MB
            dictDefaultMessage: "<small>Arrastra los archivos aqu&iacute; o da click.</small>"
        };

  var mapOptions3 = {
                center: new google.maps.LatLng(36.964645, -122.01523),
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.SATELLITE,
                // Style for Google Maps
                styles: [{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#fffffa"}]},{"featureType":"water","stylers":[{"lightness":50}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"lightness":40}]}]
            };
 var mapElement3 = document.getElementById('map3');
 var map3 = new google.maps.Map(mapElement3, mapOptions3);
 
  $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
 
</script>

<?php
include_once "footer.inc.php";
?>