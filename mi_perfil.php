<?php include_once "header.inc.php"; ?>

<div class="row wrapper border-bottom page-heading">

   <div class="col-lg-10">

      <h2><i class="fa fa-user-circle"></i> Mi perfil / Administraci&oacute;n</h2>

   </div>

   <div class="col-lg-2">

   </div>

</div>

<div class="wrapper wrapper-content">

<div class="row">

<div class="col-md-12">

   <div class="ibox-content">

      <div class="tabs-container">

         <ul class="nav nav-tabs">

            <li class="active"><a data-toggle="tab" href="#tab-1"> Datos fiscales</a></li>

            <li class=""><a data-toggle="tab" href="#tab-2">Usuarios</a></li>
			 <li class=""><a data-toggle="tab" href="#tab-3">Condiciones de venta</a></li>
			  <li class=""><a data-toggle="tab" href="#tab-4">Pol&iacute;tica de privacidad</a></li>
			  <li class=""><a data-toggle="tab" href="#tab-5">Mi perfil</a></li>

         </ul>

         <div class="tab-content">

            <div id="tab-1" class="tab-pane active">

               <div class="row" style="margin-top:20px">

                  <div class="col-md-2">

                     <div class="col-md-12">

                        <div class="profile-image">

                           <img src="img/no_logo.png" class="img-circle circle-border m-b-md" alt="profile">

                        </div>

                     </div>

                     <div class="col-md-12">

                        <div class="fileinput fileinput-new" data-provides="fileinput">

                           <span class="btn btn-default btn-file"><span class="fileinput-new">Examinar</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>

                           <span class="fileinput-filename"></span>

                           <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>

                        </div>

                     </div>

                  </div>

                  <div class="col-md-9">

                     <form class="m-t" role="form" action="mi_perfil.php" required="" method="post">

                        <div class="row" >

                           <div class="col-lg-12">

                              <div class="form-group">

                                 <label>Nombre de la empresa </label>

                                 <input id="txtNombreEmpresa" name="txtNombreEmpresa" type="text" class="form-control input-sm" required="">

                              </div>

                              <div class="form-group">

                                 <label>NIF/CIF </label>

                                 <input id="txtNIF" name="txtNIF" type="text" class="form-control input-sm"  required="">

                              </div>

                              <div class="form-group">

                                 <label>Direcci&oacute;n </label>

                                 <input id="txtDireccion" name="txtDireccion" type="text" class="form-control input-sm"  required="">

                              </div>

                           </div>

                           <div class="row" style="padding-left:15px; padding-right:15px">

                              <div class="form-group col-lg-3">

                                 <label>Ciudad</label>

                                 <input id="txtCiudad" name="txtCiudad" type="text" class="form-control input-sm"  required="">

                              </div>

                              <div class="form-group col-lg-3">

                                 <label>Provincia/Estado </label>

                                 <input id="txtProvincia" name="txtProvincia" type="text" class="form-control input-sm"  required="">

                              </div>

                              <div class="form-group col-lg-3">

                                 <label>C&oacute;digo Postal </label>

                                 <input id="txtCP" name="txtCP" type="text" class="form-control input-sm" required="">

                              </div>

                              <div class="form-group col-lg-3">

                                 <label>Pa&iacute;s</label>

                                 <input id="txtPais" name="txtPais" type="text" class="form-control input-sm" required="">

                              </div>

                              <div class="form-group col-lg-3" >

                                 <label>Tel&eacute;fono </label>

                                 <input id="txtTelefono" name="txtTelefono" type="text" class="form-control input-sm" required="">

                              </div>

                           </div>

                           <div class="row" style="margin-left:0px">

                              <div class="form-group col-lg-3" >

                                 <button type="submit" class="btn btn-primary block full-width m-b">Guardar</button>

                              </div>

                           </div>

                        </div>

                     </form>

                  </div>

               </div>

            </div>
			
			
			<div id="tab-3" class="tab-pane">
				 <div class="row">
				 <div class="col-lg-12" style="margin-top:20px">
                       
							<textarea class="form-control" rows="10"></textarea>
						</div>
				</div>
				 <div class="row" >

                              <div class="form-group col-lg-3" style="margin-top:15px" >

                                 <button type="submit" class="btn btn-primary block full-width m-b">Guardar</button>

                              </div>

                           </div>
			</div>
			
			<div id="tab-4" class="tab-pane">
				 <div class="row">
					<div class="col-lg-12" style="margin-top:20px">
                       
							Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?<br /><br />
							
							Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?<br />
						</div>
					</div>
			
			</div>
			
			 <div id="tab-5" class="tab-pane">
			 
			 
			   <form class="m-t" role="form" action="mi_perfil.php" required="" method="post">
						
                        <div class="row" >

                           <div class="col-lg-12">
                        <div class="row" >

                           <div class="col-lg-4">

                              <div class="form-group">

                                 <label>Nombre de usuario </label>

                                 <input id="txtNombreUsuario" name="txtNombreUsuario" type="text" class="form-control input-sm" required="">

                              </div>

                              <div class="form-group">

                                 <label>Contrase&ntilde;a actual</label>

                                 <input id="txtContraseniaActual" name="txtContraseniaActual" type="password" class="form-control input-sm"  required="">

                              </div>
							  
							     <div class="form-group">

                                 <label>Nueva contrase&ntilde;a</label>

                                 <input id="txtNuevaContrasenia" name="txtNuevaContrasenia" type="password" class="form-control input-sm"  required="">

                              </div>

                              <div class="form-group">

                                 <label>Confirmar contrase&ntilde;a </label>

                                 <input id="txtNuevaContraseniaConfir" name="txtNuevaContraseniaConfir" type="password" class="form-control input-sm"  required="">

                              </div>

                           </div>

                          </div>
						  </div>
						  
                           <div class="row" >

                              <div class="form-group col-lg-3" style="margin:15px">

                                 <button type="submit" class="btn btn-primary block full-width m-b">Guardar</button>

                              </div>

                           </div>

                        </div>

                     </form>


			</div>
			
            <div id="tab-2" class="tab-pane">

               <div class="row">

			   	 <div class="row" style="margin-left:0px;">
							<div class="mail-tools tooltip-demo m-t-md">
                              <div class="form-group col-lg-2" >
							
<div class="mail-tools tooltip-demo m-t-md">
                                 <button type="submit" class="btn btn-primary block full-width m-b btn-lg" data-toggle="tooltip" title="Aqu&iacute; podr&aacute;s a&ntilde;adir los usuarios que deseas que puedan utilizar el dashboard, podr&aacute;s elegir los permisos y los accesos de cada uno de ellos." ><i class="fa fa-plus" aria-hidden="true"></i> A&ntilde;adir</button>
</div>
                              </div>
							</div>
                           </div>

                  <div class="col-lg-12">

                     <div class="ibox float-e-margins">

                        <div class="table-responsive" style="margin-top:10px">

                           <table class="table table-striped table-bordered table-hover dataTables-example" >

                              <thead>

                                 <tr>

                                    <th>Usuario</th>

                                    <th>Permiso</th>

                                    <th>Sector</th>

                                    <th>Acceso</th>

                                    <th>&nbsp;</th>

                                 </tr>

                              </thead>

                              <tbody>

                                 <?php for($i=1; $i<=10; $i++){ ?>

                                 <tr class="gradeC">

                                    <td>Trident</td>

                                    <td>Internet </td>

                                    <td>Win 95+</td>

                                    <td class="center">4</td>

                                    <td class="center" style="text-align:center"> <span class="label label-primary" style="cursor:pointer">Editar</span></td>

                                 </tr>

                                 <?php }?>

                              </tbody>

                           </table>

                        </div>

						

                     </div>

                  </div>

               </div>

            </div>

         </div>

      </div>

   </div>

</div>

<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">

   <div class="modal-dialog modal-lg">

      <div class="modal-content">

         <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

            <h4 class="modal-title">Registrar / Editar Usuario</h4>

         </div>

         <div class="modal-body">

       	 <form class="m-t" role="form" action="mi_perfil.php" required="" method="post">

             <div class="row" >

                 <div class="col-lg-4">

         	         <div class="form-group">

                      <label>Usuario (Email) </label>

                      <input id="txtUsuario" name="txtUsuario" type="text" class="form-control input-sm" required="">

                     </div>   

				</div>

				<div class="col-lg-4">

         	         <div class="form-group">

                      <label>Permiso </label>

                      <select class="form-control input-sm" required="" >

					  	<option value="1">Ver</option>

						<option value="2">Ver/Editar</option>

					  </select>

                     </div>   

				</div>

				<div class="col-lg-4">

         	         <div class="form-group">

                      <label>Sector</label>

                      <select class="form-control input-sm" required="" >

					  	<option value="1">Todos</option>

						<option value="2">Vending</option>

						<option value="3">OCS</option>

					  </select>

                     </div>   

				</div>

			</div>

			<div class="row">

				<div class="col-lg-4">

                     <input type="checkbox" class="js-switch1" checked />&nbsp; P&aacute;gina principal

                 </div>

				 <div class="col-lg-4">

                     <input type="checkbox" class="js-switch10" checked />&nbsp; E-commerce

                 </div>

				 <div class="col-lg-4">

                     <input type="checkbox" class="js-switch11" checked />&nbsp; Configuraci&oacute;n

                 </div>

			</div>

			<div class="row" style="margin-top:10px">

				<div class="col-lg-12">

				<h3>Datos</h3>

				</div>

			</div>

			<div class="row">

				 <div class="col-lg-4" style="margin-top:10px">

                     <input type="checkbox" class="js-switch2" checked />&nbsp; Mi Negocio

                 </div>

				 <div class="col-lg-4" style="margin-top:10px">

                     <input type="checkbox" class="js-switch3" checked />&nbsp; Usuarios

                 </div>

				<div class="col-lg-4" style="margin-top:10px">

                     <input type="checkbox" class="js-switch4" checked />&nbsp; Estad&iacute;sticas

                 </div>

				<div class="col-lg-4" style="margin-top:10px">

                     <input type="checkbox" class="js-switch5" checked />&nbsp; Business Intelligence

                 </div>

			</div>

			

			

			<div class="row" style="margin-top:10px">

				<div class="col-lg-12">

				<h3>Comunicaci&oacute;n</h3>

				</div>

			</div>

			<div class="row" style="margin-bottom:20px">

				 <div class="col-lg-4" style="margin-top:10px">

                     <input type="checkbox" class="js-switch6" checked />&nbsp; Atenci&oacute;n al usuario

                 </div>

				 <div class="col-lg-4" style="margin-top:10px">

                     <input type="checkbox" class="js-switch7" checked />&nbsp; Gestor de promociones

                 </div>

				<div class="col-lg-4" style="margin-top:10px">

                     <input type="checkbox" class="js-switch8" checked />&nbsp; Personalizaci&oacute;n

                 </div>

				<div class="col-lg-4" style="margin-top:10px">

                     <input type="checkbox" class="js-switch9" checked />&nbsp; Business Intelligence

                 </div>

			</div>

			

		

			 <div class="modal-footer">

            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

            <button type="submit" class="btn btn-primary">Guardar</button>

         </div>

		</form>

         </div>

      </div>

   </div>

</div>

<script src="js/plugins/dataTables/datatables.min.js?v=2.33"></script>

<script src="js/plugins/switchery/switchery.js"></script>

<script>

   $(document).ready(function(){

               $('.dataTables-example').DataTable({

                   pageLength: 25,

                   responsive: true,

                   dom: '<"html5buttons"B>lTfgitp',

                   buttons: [

                       

                   ]

   

               });

			   

			   

			var elem = document.querySelector('.js-switch1');

            var switchery = new Switchery(elem, { color: '#1AB394' });

			

			var elem = document.querySelector('.js-switch2');

            var switchery = new Switchery(elem, { color: '#1AB394' });

			

			var elem = document.querySelector('.js-switch3');

            var switchery = new Switchery(elem, { color: '#1AB394' });

			

			var elem = document.querySelector('.js-switch4');

            var switchery = new Switchery(elem, { color: '#1AB394' });



			var elem = document.querySelector('.js-switch5');

            var switchery = new Switchery(elem, { color: '#1AB394' });

			

			var elem = document.querySelector('.js-switch6');

            var switchery = new Switchery(elem, { color: '#1AB394' });

            

			var elem = document.querySelector('.js-switch7');

            var switchery = new Switchery(elem, { color: '#1AB394' });

			

			var elem = document.querySelector('.js-switch8');

            var switchery = new Switchery(elem, { color: '#1AB394' });

			

			var elem = document.querySelector('.js-switch9');

            var switchery = new Switchery(elem, { color: '#1AB394' });

			

			var elem = document.querySelector('.js-switch10');

            var switchery = new Switchery(elem, { color: '#1AB394' });

			

			var elem = document.querySelector('.js-switch11');

            var switchery = new Switchery(elem, { color: '#1AB394' });

   

           });

</script>

<?php include_once "footer.inc.php"; ?>



